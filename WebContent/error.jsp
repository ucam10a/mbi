<%@ page language="java" contentType="text/html; charset=BIG5"
    pageEncoding="BIG5"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Message Page</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
</head>
<body>

<s:iterator value="vecError" id="ErrorMesg"> 
    <s:if test="success == 'true'">
        <b><font size="+1" color="#23456B"><s:property value="errormessage" /><br/><br/></font></b>
    </s:if>
    <s:else>
        <b><font size="+1" color="#FF0000"><s:property value="errormessage" /><br/><br/></font></b>
    </s:else>
</s:iterator>

</body>
</html>
