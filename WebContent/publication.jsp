<%@ page language="java" contentType="text/html; charset=BIG5"
    pageEncoding="BIG5"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title> Publication </title>
</head>
<body>
<s:if test="#session.login == 'true'">
    <s:form theme="simple" action="htmlEdit" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="type" value="publication" / > 
        <input type="submit" value="edit" />
    </s:form>
</s:if>
    <jsp:directive.include file="publicationHtml.jsp"/>

</body>
</html>