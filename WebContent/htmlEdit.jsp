<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
    <title>Edit Home Page</title>
	<meta charset="utf-8">
	<script src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
	    function getHtmlData(){
	    	var data = CKEDITOR.instances.editor1.getData();
	    	//alert(data);
	    	document.getElementById('htmlCode').value = data;
	    	document.updateHtmlFrom.submit();
	    }
	</script>
	
</head>
<body>
    <div style="width: 95%; height:600px; margin-left: auto; margin-right: auto;">
	    <textarea cols="1" rows="1" id="editor1" name="editor1" >	
            <s:property value="htmlCode" />
		</textarea>
		<script>
			CKEDITOR.replace( 'editor1' ,{
			    height: '450px',
			    resize_enabled: false
			});
		</script>
    </div>
    <s:form theme="simple" name="updateHtmlFrom" action="htmlUpdate" method="POST" enctype="multipart/form-data">
        <table width="900px" align="center">
            <tr align="center" ><td>
                <input type="hidden" name="type" value="<s:property value="type" />" / >
                <input type="hidden" id="htmlCode" name="htmlCode" value="" / >
                <input type="button" onclick="getHtmlData();" value="submit" style="align: center;" />
            </td></tr>
        </table>
    </s:form>
    
</body>
</html>
