<%@ page language="java" contentType="text/html; charset=BIG5"
    pageEncoding="BIG5"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title> Result </title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<script type="text/javascript" src="js/click.js"></script>
</head>
<body>

<script type="text/javascript">

function confirmDel(id) {
	 var expression = "Do you want to delete this paper ? ";
	 if(confirm(expression)){
		 var deleteSubmit = document.getElementById('deleteSubmit' + id);
         simulateClick(deleteSubmit, "click");
	 }
}

function showResearchTypeDescription() {
	var expression = "T: focuses on theoretical perspectives and construction of theory \n" +
                         "E: Empirical study including types specified in the next code \n" +
                         "S: synthesizing literature on MBI \n" + 
                         "D: the systematic design of a specific intervention, learning environment, or model in K-12 science education without empirical data.";
	alert(expression);
}

function showEmpiricalTypeDescription() {
    var expression = "EE: Experimental \n" +
                         "EO: Other Empirical";
    alert(expression);
}

function showTechnologyDescription() {
	
	var expression = "N: Technology is not involved in designing MBI environment \n" + 
                         "P: Personal computer-based software, which does not require the Internet access, is used for designing an MBI environment \n" +
                         "I: Internet is required in MBI \n" + 
                         "M: Mobile technology (e.g., smart phones) is involved in MBI \n" + 
                         "V: Video/film/animation (not specialized software) is used for MBI";
	alert(expression);
}

function showSubjectTypeDescription() {
    var expression = "B. Biology \n" + 
                     "C. Chemistry \n" +
                     "EA. Earth Science/astronomy \n" +
                     "E. Environmental science \n" + 
                     "P. Physics \n" +
                     "O. Other";
    alert(expression);
}

</script>

<s:if test="%{papers.size() == 0}">
    <b>No Results!</b>
</s:if>
<s:else>
    <b><s:property value="%{papers.size()}" /></b> Results <br />
    <s:url id="exportPaperExcel" namespace="/" action="exportExcel" >
        <s:param name="papers_id"><s:property value="papers_id" /></s:param>
    </s:url>
    <h3>Export excel file - <s:a href="%{exportPaperExcel}">SearchResult.xls</s:a></h3>
</s:else>
<br /><br />
<%int i = 1; %>
<s:iterator value="papers" id="paperOBJ">
    <table>
        <tr>
            <td><b><%= i %>. </b></td>
            <td>
                <s:if test="#session.login == 'true'">
                    <form id="edit" action="/MBI/edit.action" method="post">
                        <input type="hidden" name="paperId" value='<s:property value="id" />' />
                        <input type="submit" value="Edit"/>
                        <input type="button" value="Delete" onclick="confirmDel('<s:property value="id" />');" />
                    </form>
                    <form id="delete" name="deleteForm" action="/MBI/delete.action" method="post">
                        <input type="hidden" name="paperId" value='<s:property value="id" />' />
                    	<input id="deleteSubmit<s:property value="id" />" type="submit" style="display: none;" />
                    </form>
                </s:if>
            </td>
        </tr>
    </table>
    <b>Year: </b> 
    <s:if test="%{inputAttributes.contains('year')}">
    	<span style='background-color: yellow;'><s:property value="PYear" /></span><br />
    </s:if>
    <s:else>
    	<s:property value="PYear" /><br />
    </s:else>
    
    <b>Authors: </b> 
    <s:iterator value="Authors" id="author">
        <s:property value="lastName" escape="false" /> , 
        <s:property value="firstName" escape="false" /> ; 
    </s:iterator>
    <br />
    
    <b>Title: </b> <s:property value="title" escape="false" /><br />
    
    <b>Source: </b> 
    <s:property value="source" escape="false" /><br />
    
    <b>Abstract: </b> <s:property value="abstractVal" escape="false" /><br />
    
    <a href="javascript:void(null);" onclick="showResearchTypeDescription();" ><b>Research Type: </b></a>
    <s:if test="%{inputAttributes.contains('researchType')}">
    	<span style='background-color: yellow;'><s:property value="researchType" /></span><br />
    </s:if>
    <s:else>
    	<s:property value="researchType" /><br />
    </s:else>
    
    <a href="javascript:void(null);" onclick="showEmpiricalTypeDescription();" ><b>Empirical Type: </b></a>
    <s:if test="%{inputAttributes.contains('empiricalType')}">
    	<span style='background-color: yellow;'><s:property value="empiricalType" /></span><br />
    </s:if>
    <s:else>
    	<s:property value="empiricalType" /><br />
    </s:else>
    
    <a href="javascript:void(null);" onclick="showTechnologyDescription();" ><b>Technology: </b></a>
    <s:iterator value="technology" id="technology_code">
    	<s:property value="technology_code" escape="false" />,
    </s:iterator>
    <br />
    
    <b>Country: </b> 
    <s:iterator value="country" id="country_code">
        <s:property value="country_code" escape="false" /> , 
    </s:iterator>
    <br />
    
    <b>Grade: </b>
    <s:if test="%{inputAttributes.contains('grade')}"> 
        <s:if test="gradeLevel == 1.0">
            <span style='background-color: yellow;'>K-5</span>
        </s:if>
        <s:if test="gradeLevel == 2.0">
            <span style='background-color: yellow;'>6-8</span>
        </s:if>
        <s:if test="gradeLevel == 3.0">
            <span style='background-color: yellow;'>9-12</span>
        </s:if>
    </s:if>
    <s:else>
        <% boolean show = false; %>
    	<s:if test="gradeLevel == 1.0">
            K-5
            <% show = true; %>
        </s:if>
        <s:if test="gradeLevel == 2.0">
            6-8
            <% show = true; %>
        </s:if>
        <s:if test="gradeLevel == 3.0">
            9-12
            <% show = true; %>
        </s:if>
        <% if (show == false) { %>
            <%= "NA" %>
        <% } %>
    </s:else>
    <br />
    
    <a href="javascript:void(null);" onclick="showSubjectTypeDescription();" ><b>Subject: </b></a>
    <s:iterator value="subject" id="subject_code">
        <s:property value="subject_code" escape="false" /> , 
    </s:iterator>
    <br />
    
    <b>Implement Duration: </b> 
    <s:if test="%{inputAttributes.contains('implDuration')}"> 
        <span style='background-color: yellow;'><s:property value="implDuration" /></span>
    </s:if>
    <s:else>
        <s:property value="implDuration" />
    </s:else>
    <br />
    
    <br /><br />
    <% i++; %>
</s:iterator>

</body>
</html>
