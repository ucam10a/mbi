<%@ page language="java" contentType="text/html; charset=BIG5"
    pageEncoding="BIG5"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>

<title> 
    Finding <h5> &nbsp; &nbsp; Please input the values </h5>
</title>

<script type="text/javascript">

    function init() {
	    clear();
    }

    function clear() {
	
	    document.getElementById("fromYear").value = "";
	    document.getElementById("toYear").value = "";
	    document.getElementById("title").value = "";
	    document.getElementById("abstractVal").value = "";
	    document.getElementById("author").value = "";
	    document.getElementById("source").value = "";
	    document.getElementById("researchType").value = "";

	    document.getElementById("empiricalType").value = "";
	    document.getElementById("technology").value = "";
	    document.getElementById("country").value = "";
	    document.getElementById("subject").value = "";
	    document.getElementById("grade").value = "";
	    document.getElementById("fromImplDuration").value = "";
	    document.getElementById("toImplDuration").value = "";
    }
    
</script>

</head>

<body>
<script type="text/javascript">
function advancedSearch(){

	if (document.getElementById("advancedSearch").style.display == "none"){
		document.getElementById("advancedSearch").style.display = "block";
		document.getElementById("showAdvancedSearch").style.display = "none";
	}else{
		document.getElementById("advancedSearch").style.display = "none";
	}
	
}

function showDescription(div) {
	document.getElementById(div).style.display = "block";
}

function hideDescription(div) {
	document.getElementById(div).style.display = "none";
}

function comboInit(thelist)
{
	theinput = document.getElementById(theinput);  
    var idx = thelist.selectedIndex;
    var content = thelist.options[idx].innerHTML;
    if(theinput.value == "" && content != "ANY") theinput.value = content;	
}

function combo(thelist, theinput)
{
	theinput = document.getElementById(theinput);  
    var idx = thelist.selectedIndex;
    var content = thelist.options[idx].innerHTML;
    if (content != "ANY") {
    	theinput.value = content; 
    } else {
    	theinput.value = "";
    }
}

</script>
<s:form theme="simple" action="searchPaper" method="POST" enctype="multipart/form-data">
	<table>
	  	<tr>
            <td>
                Year : <br /> <input type="text" id="fromYear" name="fromYear" > to <input type="text" id="toYear" name="toYear" >
                <br />
            </td>
        </tr>
        <tr>
            <td>
                Title : <br /> <input type="text" size="40" id="title" name="title" >
                <br />
            </td>
        </tr>
        <tr>
            <td>
                Abstract : <br /> <input type="text" size="40" id="abstractVal" name="abstractVal" >
                <br />
            </td>
        </tr>
    </table>
    <div id="showAdvancedSearch" style="display:block;">
        <a href="javascript:void(null);" onclick="advancedSearch();" ><font color="#23456B" size="+0.5"><b>Advanced Search</b></font></a>
    </div>
    <div id="advancedSearch" style="display:none;" >
        <table>
            <tr>
                <td>
                    Author : <br /> <input type="text" size="40" id="author" name="author" >
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Source : <br /> <input type="text" size="40" id="source" name="source" >
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Research Type : <br />
                    <input type="text" size="40" id="researchType" name="researchType" >
                    <select size="0" onChange="combo(this, 'researchType');" onMouseOut="comboInit(this)" value="" >
                        <option value="" >ANY</option>
                        <option value="T" >T</option>
                        <option value="E" >E</option>
                        <option value="S" >S</option>
                        <option value="D" >D</option>
                    </select>
                    <a href="javascript:void(null);" onclick="showDescription('researchTypeDesc');" ><font color="#23456B" size="+0.5"><b>show description</b></font></a>
                    <div id="researchTypeDesc" style="display: none;">
                        T: focuses on theoretical perspectives and construction of theory <br />
                        E: Empirical study including types specified in the next code <br />
                        S: synthesizing literature on MBI <br />
                        D: the systematic design of a specific intervention, learning environment, or model in K-12 science education without empirical data. <br />
                    </div>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Empirical Type : <br /> 
                    <input type="text" size="40" id="empiricalType" name="empiricalType" >
                    <select size="0" onChange="combo(this, 'empiricalType');" onMouseOut="comboInit(this)" value="" >
                        <option value="" >ANY</option>
                        <option value="EE" >EE</option>
                        <option value="EO" >EO</option>
                    </select>
                    <a href="javascript:void(null);" onclick="showDescription('empiricalTypeDesc');" ><font color="#23456B" size="+0.5"><b>show description</b></font></a>
                    <div id="empiricalTypeDesc" style="display: none;">
                        EE: Experimental <br />
                        EO: Other Empirical <br />
                    </div>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Technology : <br />
                    <input type="text" size="40" id="technology" name="technology" >
                    <select size="0" onChange="combo(this, 'technology');" onMouseOut="comboInit(this)" value="" >
                        <option value="" >ANY</option>
                        <option value="N" >N</option>
                        <option value="P" >P</option>
                        <option value="I" >I</option>
                        <option value="M" >M</option>
                        <option value="V" >V</option>
                    </select>
                    <a href="javascript:void(null);" onclick="showDescription('technologyDesc');" ><font color="#23456B" size="+0.5"><b>show description</b></font></a>
                    <div id="technologyDesc" style="display: none;">
                        N: Technology is not involved in designing MBI environment <br />
                        P: Personal computer-based software, which does not require the Internet access, is used for designing an MBI environment <br />
                        I: Internet is required in MBI <br />
                        M: Mobile technology (e.g., smart phones) is involved in MBI <br />
                        V: Video/film/animation (not specialized software) is used for MBI <br/>
                    </div>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Country : <br /> <input type="text" size="40" id="country" name="country" >
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Subject : <br />
                    <input type="text" size="40" id="subject" name="subject" >
                    <select size="0" onChange="combo(this, 'subject');" onMouseOut="comboInit(this)" value="" >
                        <option value="" >ANY</option>
                        <option value="B" >B</option>
                        <option value="C" >C</option>
                        <option value="EA" >EA</option>
                        <option value="E" >E</option>
                        <option value="P" >P</option>
                        <option value="O" >O</option>
                    </select>
                    <a href="javascript:void(null);" onclick="showDescription('subjectDesc');" ><font color="#23456B" size="+0.5"><b>show description</b></font></a>
                    <div id="subjectDesc" style="display: none;">
                        B. Biology <br />
                        C. Chemistry <br />
                        EA. Earth Science/astronomy <br />
                        E. Environmental science <br />
                        P. Physics <br/>
                        O. Other <br />
                    </div>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        Grade Level : <br />
                        <select size="0" name="grade" value="" >
                            <option value="" >ANY</option>
                            <option value="1" >K-5</option>
                            <option value="2" >6-8</option>
                            <option value="3" >9-12</option>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        Implement Duration : 
                        <br />
                        <input type="text" id="fromImplDuration" name="fromImplDuration" > to <input type="text" id="toImplDuration" name="toImplDuration" >
                        <a href="javascript:void(null);" onclick="showDescription('implDurationDesc');" ><font color="#23456B" size="+0.5"><b>show description</b></font></a>
                        <div id="implDurationDesc" style="display: none;">
                            0. Not reported <br />
                            1. 0-3 hrs <br />
                            2. 3-10 hrs <br />
                            3. more than 10 hrs <br />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <s:submit type="button"  />
</s:form>


</body>
</html>