<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ACMBI in Science Education</title>

<link href="./style/style.css" rel="stylesheet" type="text/css" />

<sitemesh:write property='head'/> 

</head>
<% 
    String login = (String) session.getAttribute("login"); 
    String userID = (String) session.getAttribute("userID"); 
    if (login == null) login = "";
%>

<body class="home page page-id-31 page-template-default logged-in admin-bar two_col_left two-columns" onload="init();">
    <div class="bg-gradient">
        <div id="container" class="container_16">
            <div id="top-bar">
                <div id="profiles" class="clearfix gutter-left">
                    <a id="social-id-1" class="mysocial social-rss" title="Subscribe to MBI's RSS feed" href="#">
                        <img title="Subscribe to MBI's RSS feed" alt="RSS" src="./images/social/rss.png" />
                    </a>
                </div>
                <div id="top_search" class="grid_4">
                    <form id="searchform" class="searchform" action="http://localhost:8080/MBI" method="get">
                        <p class="clearfix default_searchform">
                             <input type="text" value="Search" onfocus="if (this.value == 'Search') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search';}" name="s" />
                             <button type="submit" >
                                 <span>Search</span>
                             </button>
                        </p>
                    </form>
                </div>
            </div>

            <div id="header">
                <img class="header-img" alt="" src="./images/headers/flow.jpg" />
                <h1 class="header_title push_1 grid_15"> ACMBI in Science Education </h1>
                <h2 class="header_desc push_1 grid_15">	Achievement and Challenges of Modeling-Based Instruction in Science Education from 1980 to 2009 </h2>
            </div>

            <div id="nav">
                <div id="header-menu-wrap" >
                    <ul id="header-menu" >
                        <li>
                            <a href="index.jsp">
                                <strong> Home </strong>
                            </a>
                        </li>
                        <li>
                            <a href="introduction.jsp">
                                <strong>Introduction</strong>
                            </a>
                        </li>
                        <li>
                            <a href="search.jsp">
                                <strong>MBI Library</strong>
                            </a>
                        </li>
                        <% if (!login.equals("")){ %>
                            <li>
                                <a href="getNewData.action">
                                    <strong>Insert</strong>
                                </a>
                            </li>
                        <% } %>
                        <li>
                            <a href="team.jsp">
                                <strong>Team</strong>
                            </a>
                        </li>
                        <li>
                            <a href="publication.jsp">
                                <strong>Publication</strong>
                            </a>
                        </li>
                        <li>
                            <a href="contact.jsp">
                                <strong>Contact</strong>
                            </a>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="menu-bottom-shadow"> </div> 
            </div>
         
            <div id="content" class="clearfix hfeed">
                <div id="content-main" class="clearfix grid_11">
                    <div class="featured_slider clearfix thumbnail-excerpt horizontal-slide">
                        <div id="slider_root" class="clearfix">
                            <div id="slider_text">
                                <h3><sitemesh:write property='title'/></h3>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="sidebar1" class="sidebar grid_5">
                    <% if (login.equals("")){ %>
                        <form id="login" action="/MBI/login.action" method="post">
                            <h3>Administrator Login</h3>
                            <table style="width: 100%; background: #fbfbfb;">
                        	    <tr>
                        		    <td><p>Username</p></td><td><input id="text1" type="text" name="userID" value="" size="20" /></td>
                        	    </tr>
                        	    <tr>
                        	        <td><p>Password</p></td><td><input id="text2" type="password" name="password" value="" size="20" /></td>
                        	    </tr>
                        	    <tr>
                        		    <td colspan="2" align="right" ><input type="submit" value="Submit"/></td>
                        	    </tr>
                            </table>
           			    </form>
                    <% } else { %>
                        <form id="logout" action="/MBI/logout.action" method="post">
                            <h3>Welcome Administrator ! <%= userID %></h3>
                            <table style="width: 100%; background: #fbfbfb;">
                        	    <tr>
                        		    <td> 
                        		        <a href="changePass.jsp">
                        		            &nbsp; &nbsp; Change Password
                                        </a>
                                    </td>
                        	    </tr>
                        	    <tr>
                        	        <td>
                        	            <a href="#">
                        		            &nbsp; &nbsp; Forget Password
                                        </a>
                        	        </td>
                        	    </tr>
                        	    <tr>
                        		    <td colspan="2" align="right" ><input type="submit" value="Logout"/></td>
                        	    </tr>
                            </table>
           			    </form>
                    <% } %>
                </div>
                
                
            
            </div>
            
            <div id="content-center" >
                <br />
                <sitemesh:write property='body'/> 
            </div>
            
            <div id="footer" class="clearfix">
                <div id="copyright">
                    <h3>Copyright</h3>
		            <p> &copy; 2012 ACMBI in Science Education. </p>
                </div>
                <div class="footer-menu-wrap">
    	            <ul id="footer-menu" class="clearfix">
			             <li class="menu-item return-top"><a href="#">Return to top</a></li>
                    </ul>
                </div>
    	        <div id="developer" class="grid_7">
                    <p> Powered by <a href="http://wordpress.org/">WordPress</a> and the <a href="http://www.khairul-syahir.com/wordpress-dev/graphene-theme">Graphene Theme</a>.        </p>
                </div>
            </div>
            
         </div>
    </div>
</body>
</html>
