<%@ page language="java" contentType="text/html; charset=BIG5"
    pageEncoding="BIG5"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title> Change password </title>
</head>
<body>
    <s:form action="changePass" theme="simple" method="POST">
	    <table width="537">	
            <tr>
                <td height="48" colspan="2" align="center"><font size="+2" color="#23456B"><b>Change Password</b></font></td>
            </tr>
            <tr>
                <td height="25">
                    <font size="+1" color="#23456B">Password</font></td><td><input id="text2" type="password" name="oldPassword" value="" size="30"/>
                    <br/>
                    <s:iterator value="passwordError" id="ErrorMesg">
                        <font color="#FF0000"><s:property value="errormessage" /></font>
                    </s:iterator>
                </td>
            </tr>
            <tr>
                <td height="25">
                    <font size="+1" color="#23456B">New Password</font></td><td><input id="text2" type="password" name="newPassword" value="" size="30"/>
                    <br/>
                    <s:iterator value="newPsswordError" id="ErrorMesg">
                        <font color="#FF0000"><s:property value="errormessage" /></font>
                    </s:iterator>
                </td>
            </tr>
            <tr>
                <td height="25">
                    <font size="+1" color="#23456B">Type New Password again</font>
                </td>
                <td>
                    <input id="text3" type="password" name="confirmNewPassword" value="" size="30"/>
                    <br/>
                    <s:iterator value="newPsswordError" id="ErrorMesg">
                        <font color="#FF0000"><s:property value="errormessage" /></font>
                    </s:iterator>
                </td>
            </tr>
            <tr>
                <td height="25">
                    <font size="+1" color="#23456B">Email</font>
                </td>
                <td>
                    <input id="text4" type="text" name="email" value="" size="30"/>
                    <br/>
                    <s:iterator value="emailError" id="ErrorMesg">
                        <font color="#FF0000"><s:property value="errormessage" /></font>
                    </s:iterator>
                </td>
            </tr>
            <tr>
                <td height="25"></td><td><s:submit type="submit" /></td>
            </tr>	
        </table>
    </s:form>
</body>
</html>