<%@ page language="java" contentType="text/html; charset=BIG5"
    pageEncoding="BIG5"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title> Upload </title>
</head>
<body>

This is for Excel xlsx file to insert to database.
<s:form theme="simple" action="insertXlsx" method="POST" enctype="multipart/form-data">
	<table>
	  	<tr>
	  		<td valign="top" height="20" colspan="2">
	  		    <input type="file" name="file" id="file">
            </td>
	  	</tr>
	  	<tr>
	  	    <td>
	  	        <s:submit type="button"  />
	  	    </td>
	  	</tr>
    </table>
</s:form>


</body>
</html>