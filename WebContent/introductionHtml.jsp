<div class="entry-content clearfix">
        <p>This two-year synthesis project aims to provide a comprehensive review of the research and practices for modeling-based instruction (MBI) in K-12 science education over the last three decades. We will rigorously code and examine the literature to conceptualize the landscape of the theoretical frameworks of MBI approaches, identify the effective design features of modeling-based learning environments with an emphasis on those that are technology-enhanced ones, and identify the most effective MBI practices that are associated with successful student learning through a meta-analysis. The proposed project will create a framework for the science education community to better understand and to effectively implement modeling-based science teaching and learning. Our rigorous and innovative dissemination strategies will help the research results reach a broad range of audiences including science education researchers, practicing teachers, and policy makers.</p>
        <p>
            For more general information, please check NSF website. [
            <a href="http://www.nsf.gov/awardsearch/showAward?AWD_ID=1019866&HistoricalAwards=false">View</a>
            ]
        </p>
    </div>