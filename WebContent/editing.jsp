<%@ page language="java" contentType="text/html; charset=BIG5"
    pageEncoding="BIG5"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title> Editing </title>
</head>
<body>
<script type="text/javascript">
function showDescription(div) {
	document.getElementById(div).style.display = "block";
}

function hideDescription(div) {
	document.getElementById(div).style.display = "none";
}

function addAuthor(){
	var newAuthorDiv = document.getElementById('newAuthor');
	newAuthorDiv.innerHTML = newAuthorDiv.innerHTML + 
	    '<div>' +
	    'first name: <input type="text" size="15" id="firstName" name="firstName" > ' + 
        'last name: <input type="text" size="15" id="lastName" name="lastName" > ' + 
        '<a href="javascript:void(null);" onclick="remove(this, \'Author\');" ><b>remove</b></a>' +
        '</div>';
}

function addTechnology(){
	var newTechDiv = document.getElementById('newTechnologyCode');
	newTechDiv.innerHTML = newTechDiv.innerHTML + 
	    '<div>' +
	    '<select name="technologyCode" size="0" >' + 
	    '<option value="" >ANY</option>' + 
	    '<option value="N" >N</option>' +
	    '<option value="P" >P</option>' +
	    '<option value="I" >I</option>' +
	    '<option value="M" >M</option>' +
	    '<option value="V" >V</option>' +
	    '</select> ' +
        '<a href="javascript:void(null);" onclick="remove(this, \'Author\');" ><b>remove</b></a>' +
        '</div>';
}

function addCountry(){
	var newCountryDiv = document.getElementById('newCountryCode');
	newCountryDiv.innerHTML = newCountryDiv.innerHTML + 
	    '<div>' +
        'country code: <input type="text" size="20" id="country" name="countryCode" /> ' +
        '<a href="javascript:void(null);" onclick="remove(this, \'Country\');" ><b>remove</b></a> ' +
        '</div>';
}

function addSubject(){
	var newSubjectDiv = document.getElementById('newSubjectCode');
	newSubjectDiv.innerHTML = newSubjectDiv.innerHTML + 
	    '<div>' +
	    '<select name="subjectCode" size="0" >' + 
	    '<option value="" >ANY</option>' +
        '<option value="B" >B</option>' +
        '<option value="C" >C</option>' +
        '<option value="EA" >EA</option>' +
        '<option value="E" >E</option>' +
        '<option value="P" >P</option>' +
        '<option value="O" >O</option>' +
	    '</select> ' +
        '<a href="javascript:void(null);" onclick="remove(this, \'Author\');" ><b>remove</b></a>' +
        '</div>';
}

function remove(aTag, type){
    var expression = "Do you want to remove this " + type + " ? ";
    if(confirm(expression)){
        var removeTag = aTag.parentNode;
    	var parent = removeTag.parentNode;
	    parent.removeChild(removeTag);
	}
}

</script>

<h3>Paper information Edit : </h3>

<s:iterator value="vecError" id="ErrorMesg"> 
    <s:if test="success == 'true'">
        <b><font size="+1" color="#23456B"><s:property value="errormessage" /><br/><br/></font></b>
    </s:if>
    <s:else>
        <b><font size="+1" color="#FF0000"><s:property value="errormessage" /><br/><br/></font></b>
    </s:else>
</s:iterator>

<s:form action="update" theme="simple" method="POST">
    <s:iterator value="papers" id="paperOBJ">
        <% boolean selected = false; %>
        <input type="hidden" name="paperId" value='<s:property value="id" />' /> 
        <table>
            <tr>
                <td>
                    Year : <br /> <input type="text" id="year" name="year" value='<s:property value="PYear" />' >
                </td>
            </tr>
            <tr>
                <td>
                    Title : <br /> <textarea name="title" id="title" cols="100"; rows="5" ><s:property value="title" /></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    Abstract : <br /> <textarea name="abstractVal" id="abstractVal" cols="100"; rows="5" ><s:property value="abstractVal" /></textarea> 
                    <br />
                </td>
            </tr>
            <tr>
            	<td>
            	    Author : <a href="javascript:void(null);" onclick="addAuthor();" ><b>add another new one</b></a>
            	</td>
            </tr>
            <s:iterator value="Authors" id="author">
                <tr>
                    <td>
                        <div>
                            first name: <input type="text" size="15" id="firstName" name="firstName" value='<s:property value="firstName" />' >
                            last name: <input type="text" size="15" id="lastName" name="lastName" value='<s:property value="lastName" />' >
                            <a href="javascript:void(null);" onclick="remove(this, 'Author');" ><b>remove</b></a>
                        </div>
                    </td>
                </tr>
            </s:iterator>
            <tr>
            	<td>
            		<div id="newAuthor" >
            	    </div>
            	</td>
            </tr>
            <tr>
                <td>
                    Source : <br /> <textarea name="source" id="source" cols="50"; rows="5" ><s:property value="source" /></textarea> 
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Research Type : 
                    <br />
                    <select name="researchType" >
                        <% selected = false; %>
                        <s:if test="researchType == \"T\"">
                            <option value="T" selected="selected" >T</option>
                            <% selected = true; %>
                        </s:if>
                        <s:else>
                            <option value="T" >T</option>
                        </s:else>
                        <s:if test="researchType == \"E\"">
                            <option value="E" selected="selected" >E</option>
                            <% selected = true; %>
                        </s:if>
                        <s:else>
                            <option value="E" >E</option>
                        </s:else>
                        <s:if test="researchType == \"S\"">
                            <option value="S" selected="selected" >S</option>
                            <% selected = true; %>
                        </s:if>
                        <s:else>
                            <option value="S" >S</option>
                        </s:else>
                        <s:if test="researchType == \"D\"">
                            <option value="D" selected="selected" >D</option>
                            <% selected = true; %>
                        </s:if>
                        <s:else>
                            <option value="D" >D</option>
                        </s:else>
                        <% if (selected == false){ %>
                            <option value="NA" selected="selected" >NA</option>
                        <% } %>
                    </select>
                    <a href="javascript:void(null);" onclick="showDescription('researchTypeDesc');" ><font color="#23456B" size="+0.5"><b>show description</b></font></a>
                    <div id="researchTypeDesc" style="display: none;">
                        T: focuses on theoretical perspectives and construction of theory <br />
                        E: Empirical study including types specified in the next code <br />
                        S: synthesizing literature on MBI <br />
                        D: the systematic design of a specific intervention, learning environment, or model in K-12 science education without empirical data. <br />
                    </div>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Empirical Type : <br /> 
                    <select name="empiricalType" >
                        <% selected = false; %>
                        <s:if test="empiricalType == \"EE\"">
                            <option value="EE" selected="selected" >EE</option>
                            <% selected = true; %>
                        </s:if>
                        <s:else>
                            <option value="EE" >EE</option>
                        </s:else>
                        <s:if test="empiricalType == \"EO\"">
                            <option value="EO" selected="selected" >EO</option>
                            <% selected = true; %>
                        </s:if>
                        <s:else>
                            <option value="EO" >EO</option>
                        </s:else>
                        <% if (selected == false){ %>
                            <option value="NA" selected="selected" >NA</option>
                        <% } %>
                    </select>
                    <a href="javascript:void(null);" onclick="showDescription('empiricalTypeDesc');" ><font color="#23456B" size="+0.5"><b>show description</b></font></a>
                    <div id="empiricalTypeDesc" style="display: none;">
                        EE: Experimental <br />
                        EO: Other Empirical <br />
                    </div>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
            	    Technology : <a href="javascript:void(null);" onclick="addTechnology();" ><b>add another new one</b></a> &nbsp; &nbsp; <a href="javascript:void(null);" onclick="showDescription('technologyDesc');" ><font color="#23456B" size="+0.5"><b>show description</b></font></a>
            	</td>
            </tr>
            <s:iterator value="technology" id="technology_code">
                <tr>
                    <td>
                        <div> 
                            <select name="technologyCode" >
                                <s:if test="#technology_code == \"N\"">
                                    <option value="N" selected="selected" >N</option>
                                </s:if>
                                <s:else>
                                    <option value="N" >N</option>
                                </s:else>
                                <s:if test="#technology_code == \"P\"">
                                    <option value="P" selected="selected" >P</option>
                                </s:if>
                                <s:else>
                                    <option value="P" >P</option>
                                </s:else>
                                <s:if test="#technology_code == \"I\"">
                                    <option selected="selected" value="I" >I</option>
                                </s:if>
                                <s:else>
                                    <option value="I" >I</option>
                                </s:else>
                                <s:if test="#technology_code == \"M\"">
                                    <option value="M" selected="selected" >M</option>
                                </s:if>
                                <s:else>
                                    <option value="M" >M</option>
                                </s:else>
                                <s:if test="#technology_code == \"V\"">
                                    <option value="V" selected="selected" >V</option>
                                </s:if>
                                <s:else>
                                    <option value="V" >V</option>
                                </s:else>
                                <s:if test="#technology_code == \"NA\"">
                                    <option value="NA" selected="selected" >NA</option>
                                </s:if>
                                <s:else>
                                    <option value="NA" >NA</option>
                                </s:else>
                            </select>
                            <a href="javascript:void(null);" onclick="remove(this, 'Technology');" ><b>remove</b></a>
                        </div>
                    </td>
                </tr>
            </s:iterator>
            <tr>
            	<td>
            	    <div id="newTechnologyCode" >
            	    </div>
            	    <div id="technologyDesc" style="display: none;">
                        N: Technology is not involved in designing MBI environment <br />
                        P: Personal computer-based software, which does not require the Internet access, is used for designing an MBI environment <br />
                        I: Internet is required in MBI <br />
                        M: Mobile technology (e.g., smart phones) is involved in MBI <br />
                        V: Video/film/animation (not specialized software) is used for MBI <br/>
                    </div>
            		<br />
            	</td>
            </tr>
            <tr>
                <td>
            	    Country : <a href="javascript:void(null);" onclick="addCountry();" ><b>add another new one</b></a>
            	</td>
            </tr>
            <s:iterator value="country" id="country_code">
                <tr>
                    <td>
                        <div>
                            country code: <input type="text" size="20" id="country" name="countryCode" value='<s:property value="country_code" />' >
                            <a href="javascript:void(null);" onclick="remove(this, 'Country');" ><b>remove</b></a>
                        </div>
                    </td>
                </tr>
            </s:iterator>
            <tr>
            	<td>
            		<div id="newCountryCode" >
            	    </div>
            	</td>
            </tr>
            <tr>
                <td>
            	    Subject : <a href="javascript:void(null);" onclick="addSubject();" ><b>add another new one</b></a> &nbsp; &nbsp; <a href="javascript:void(null);" onclick="showDescription('subjectDesc');" ><font color="#23456B" size="+0.5"><b>show description</b></font></a>
            	</td>
            </tr>
            <s:iterator value="subject" id="subject_code">
                <tr>
                    <td>
                        <select name="subjectCode" >
                            <s:if test="#subject_code == \"B\"">
                                <option value="B" selected="selected" >B</option>
                            </s:if>
                            <s:else>
                                <option value="B" >B</option>
                            </s:else>
                            <s:if test="#subject_code == \"C\"">
                                <option value="C" selected="selected" >C</option>
                            </s:if>
                            <s:else>
                                <option value="C" >C</option>
                            </s:else>
                            <s:if test="#subject_code == \"EA\"">
                                <option selected="selected" value="EA" >EA</option>
                            </s:if>
                            <s:else>
                                <option value="EA" >EA</option>
                            </s:else>
                            <s:if test="#subject_code == \"E\"">
                                <option value="E" selected="selected" >E</option>
                            </s:if>
                            <s:else>
                                <option value="E" >E</option>
                            </s:else>
                            <s:if test="#subject_code == \"P\"">
                                <option value="P" selected="selected" >P</option>
                            </s:if>
                            <s:else>
                                <option value="P" >P</option>
                            </s:else>
                            <s:if test="#subject_code == \"O\"">
                                <option value="O" selected="selected" >O</option>
                            </s:if>
                            <s:else>
                                <option value="P" >O</option>
                            </s:else>
                            <s:if test="#subject_code == \"NA\"">
                                <option value="NA" selected="selected" >NA</option>
                            </s:if>
                            <s:else>
                                <option value="NA" >NA</option>
                            </s:else>
                        </select>
                        <a href="javascript:void(null);" onclick="remove(this, 'Subject');" ><b>remove</b></a>
                    </td>
                </tr>
            </s:iterator>
            <tr>
            	<td>
            	    <div id="newSubjectCode" >
            	    </div>
            	    <div id="subjectDesc" style="display: none;">
                        B. Biology <br />
                        C. Chemistry <br />
                        EA. Earth Science/astronomy <br />
                        E. Environmental science <br />
                        P. Physics <br/>
                        O. Other <br />
                    </div>
            		<br />
            	</td>
            </tr>
            <tr>
                <td>
                    Grade Level : <br />
                    <select name="gradeLevel" >
                        <s:if test="gradeLevel == 1.0">
                            <option value="1" selected="selected" >K-5</option>
                        </s:if>
                        <s:else>
                            <option value="1" >K-5</option>
                        </s:else>
                        <s:if test="gradeLevel == 2.0">
                            <option value="2" selected="selected" >9-12</option>
                        </s:if>
                        <s:else>
                            <option value="2" >6-8</option>
                        </s:else>
                        <s:if test="gradeLevel == 3.0">
                            <option value="3" selected="selected" >9-12</option>
                        </s:if>
                        <s:else>
                            <option value="3" >K-5</option>
                        </s:else>
                        <s:if test="gradeLevel == 0">
                            <option value="0" selected="selected" >NA</option>
                        </s:if>
                        <s:else>
                            <option value="0" >NA</option>
                        </s:else>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Implement Duration : 
                    <br />
                    <input type="text" name="implDuration" value='<s:property value="implDuration" />' >
                </td>
            </tr>
        </table>
    </s:iterator>
    
    <br /> 
    <s:submit />
    
</s:form>

</body>
</html>