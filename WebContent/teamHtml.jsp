<div class="entry-content clearfix">
    <p><strong><a href="https://sites.google.com/site/shenresearch/Home" target="_blank">Ji Shen</a>, Assistant Professor<a href="images/Ji.jpg"><img class="alignright size-medium wp-image-43" title="Ji" src="images/Ji-300x225.jpg" alt="" width="300" height="225" /></a></strong></p>
    <address><strong>Mathematics &amp; Science Education</strong></address>
    <address><strong>The University of Georgia</strong></address>
    <p>Ji Shen is an Assistant Professor in the Mathematics and Science Education department at the University of Georgia. He received a B.S. degree in Geophysics from Peking University. Ji received his Ph.D. in physics (education) from Washington University in St. Louis. He had a 2-year post-doctoral research experience at the University of California, Berkeley before joined the University of Georgia. Ji is leading the Transformative Modeling research group.</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p><span id="more-9"></span></p>
    <p><strong><a href="http://faculty.soe.syr.edu/jlei/" target="_blank">Jing Lei</a>, Associate Professor<a href="images/Jing.png"><img class="alignright size-full wp-image-41" title="Jing" src="images/Jing.png" alt="" width="142" height="198" /></a></strong></p>
    <address><strong>Instructional Design, Development &amp; Evaluation</strong></address>
    <address><strong>Syracuse University</strong></address>
    <p>Jing Lei&#8217;s research interests include educational technology integration, meaningful technology use in schools, social-cultural and psychological impact of technology, teacher technology professional development, and international and comparative education. Her most recent research concerns how the use of technology both influences and is influenced by teachers, students, and school systems. Her research papers appear in such journals as Education Review, Teachers College Record, and Computer and Education. She teaches courses in instructional design, development and evaluation. She holds a masters degree in higher education and comparative education from Peking University and the Ph.D. in learning, technology and culture from Michigan State University. She served as a Chinese language instructor at Tsinghua University and has taught at Luoyang Teachers College, Weishi Junior Teachers College, and Henan University Affiliated elementary School.</p>
    <p><!--more--></p>
    <p>Ji Shen, Assistant</p>
</div>

