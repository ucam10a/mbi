package mpi.orm;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * 
 * @author Long
 * 
 * This object is used to describe the paper, it includes every attribute of the paper
 *
 */
public class PaperOBJ {

	/**
	 * paper id
	 */
	private int id = -1;
	
	/**
	 * the publish year
	 */
	private int PYear;
	
	/**
	 * every author name
	 */
	private ArrayList<Author> Authors = new ArrayList<Author>();
	/**
	 * used to compare
	 */
	private ArrayList<String> AuthorFullNames = new ArrayList<String>();
	
	/**
	 * the title of the paper
	 */
	private String title = "";
	
	/**
	 * the source of paper
	 */
	private String source = "";
	
	/**
	 * the abstract of paper
	 */
	private String abstractVal = "";
	
	/**
	 * the research type of the paper
	 */
	private String researchType = "";
	
	/**
	 * the empirical type of paper
	 */
	private String empiricalType = "";
	
	/**
	 * the technology used in this paper
	 */
	private ArrayList<String> technology = new ArrayList<String>();
	/**
	 * used to compare
	 */
	private ArrayList<String> techCodes = new ArrayList<String>();
	
	/**
	 * the country or region of the paper
	 */
	private ArrayList<String> country = new ArrayList<String>();
	/**
	 * used to compare
	 */
	private ArrayList<String> countryCodes = new ArrayList<String>();
	
	/**
	 * the grade level of paper
	 */
	private String gradeLevel = "";
	
	/**
	 * the subject of the paper
	 */
	private ArrayList<String> subject = new ArrayList<String>();
	/**
	 * used to compare
	 */
	private ArrayList<String> subjectCodes = new ArrayList<String>();
	
	/**
	 * the implement duration of the paper
	 */
	private int implDuration = 0;
	
	/**
	 * title MD5 hash code which is used to check title if unique
	 */
	private String TitleMD5;

	
	public void setPYear(int pYear) {
		PYear = pYear;
	}

	public int getPYear() {
		return PYear;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSource() {
		return source;
	}

	public void setAbstractVal(String abstractVal) {
		this.abstractVal = abstractVal;
	}

	public String getAbstractVal() {
		return abstractVal;
	}

	public void setResearchType(String researchType) {
		this.researchType = researchType;
	}

	public String getResearchType() {
		return researchType;
	}

	public void setEmpiricalType(String empiricalType) {
		this.empiricalType = empiricalType;
	}

	public String getEmpiricalType() {
		return empiricalType;
	}

	public void setGradeLevel(String gradeLevel) {
		this.gradeLevel = gradeLevel;
	}

	public String getGradeLevel() {
		return gradeLevel;
	}

	public void setImplDuration(int implDuration) {
		this.implDuration = implDuration;
	}

	public int getImplDuration() {
		return implDuration;
	}

	public void setAuthors(ArrayList<Author> authors) {
		Authors = authors;
	}

	public ArrayList<Author> getAuthors() {
		return Authors;
	}

	public void setCountry(ArrayList<String> country) {
		this.country = country;
	}

	public ArrayList<String> getCountry() {
		return country;
	}

	public void setTechnology(ArrayList<String> technology) {
		this.technology = technology;
	}

	public ArrayList<String> getTechnology() {
		return technology;
	}

	public void setSubject(ArrayList<String> subject) {
		this.subject = subject;
	}

	public ArrayList<String> getSubject() {
		return subject;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
	
	public void setAuthors(String authorGroup) {
		ArrayList<Author> AuthorGroup = new ArrayList<Author>();
		String[] authors = authorGroup.split(";");
		
		HashSet<String> editors = new HashSet<String>();
		
		for (String author : authors){
			String[] names = author.trim().split(",", 2);
			Author editor = new Author();
			String name = "";
			if (names.length > 1) {
				editor.setFirstName(names[1]);
				name += names[1];
			}
			editor.setLastName(names[0]);
			name += names[0];
			if (!editors.contains(name)){
				editor.setAuthorOrder(AuthorGroup.size() + 1);
				AuthorGroup.add(editor);
				editors.add(name);
			}
		}
		setAuthors(AuthorGroup);
	}
	
	public void setTechnology (String technology) {
		ArrayList<String> tech_codes = new ArrayList<String>();
		String[] codes = technology.split(",");
		for (String code : codes) tech_codes.add(code.toUpperCase());
		setTechnology(tech_codes);
	}
	
	public void setCountry (String country) {
		ArrayList<String> country_codes = new ArrayList<String>();
		String[] codes = country.split(",");
		for (String code : codes) country_codes.add(code);
		setCountry(country_codes);
	}
	
	public void setSubject (String subject) {
		ArrayList<String> subject_codes = new ArrayList<String>();
		String[] codes = subject.split(",");
		for (String code : codes) subject_codes.add(code);
		setSubject(subject_codes);
	}

	public void setTitleMD5(String titleMD5) {
		TitleMD5 = titleMD5;
	}

	public String getTitleMD5() {
		return TitleMD5;
	}

	public void setAuthorFullNames(ArrayList<String> authorFullNames) {
		AuthorFullNames = authorFullNames;
	}

	public ArrayList<String> getAuthorFullNames() {
		return AuthorFullNames;
	}

	public void setTechCodes(ArrayList<String> techCodes) {
		this.techCodes = techCodes;
	}

	public ArrayList<String> getTechCodes() {
		return techCodes;
	}

	public void setCountryCodes(ArrayList<String> countryCodes) {
		this.countryCodes = countryCodes;
	}

	public ArrayList<String> getCountryCodes() {
		return countryCodes;
	}

	public void setSubjectCodes(ArrayList<String> subjectCodes) {
		this.subjectCodes = subjectCodes;
	}

	public ArrayList<String> getSubjectCodes() {
		return subjectCodes;
	}

	public void addAuthor(Author author) {
		int currentIdx = 0;
		for (Author a : Authors){
			if (a.getAuthorOrder() > author.getAuthorOrder()){
			    break;
			}
			currentIdx++;
		}
		Authors.add(currentIdx, author);
	}

}
