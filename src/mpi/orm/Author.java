package mpi.orm;

/**
 * Define the author object
 * @author Long
 */
public class Author {

	/** author first name */
	private String firstName = "";
	
	/** author last name */
	private String lastName = "";
	
	/** author last name */
	private int AuthorOrder = -1;

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setAuthorOrder(int authorOrder) {
		AuthorOrder = authorOrder;
	}

	public int getAuthorOrder() {
		return AuthorOrder;
	}
	
}
