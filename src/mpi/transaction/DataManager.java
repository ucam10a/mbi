package mpi.transaction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeSet;

import mpi.orm.Author;
import mpi.orm.PaperOBJ;
import mpi.util.Stemmer;


/**
 * 
 * @author Long
 * The class which is responsible for inserting, delete and update data  
 */
public class DataManager {

	/** sql connection */
	public Connection conn;
	
	/** the stop words */
	public static TreeSet<String> stopWords = new TreeSet<String>();
	
	/**
	 * Stemming word object, for example, books -> book, writing -> write, wrote -> write
	 */
	public static Stemmer stemmer = new Stemmer();
	
	
	/**
	 * constructor
	 * @param _conn
	 */
	public DataManager(Connection _conn){
		this.conn = _conn;
	}
	
	
	/**
	 * Insert paper to database
	 * @param paper the inserted paper
	 * @throws SQLException
	 */
	public void insertPaper(PaperOBJ paper) throws SQLException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "INSERT INTO paper VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? );";
		
		pstmt = conn.prepareStatement(SQL);
		
		pstmt.setString(1, null);
		pstmt.setString(2, paper.getTitle());
		pstmt.setString(3, paper.getAbstractVal());
		pstmt.setString(4, paper.getSource());
		pstmt.setInt(5, paper.getPYear());
		pstmt.setString(6, paper.getResearchType());
		pstmt.setString(7, paper.getEmpiricalType());
		pstmt.setString(8, paper.getGradeLevel());
		pstmt.setInt(9, paper.getImplDuration());
		pstmt.setString(10, paper.getTitleMD5());
		pstmt.executeUpdate();
		
		
	}
	
	/**
	 * update the paper information in database
	 * @param paper paper
	 * @throws SQLException
	 */
	public void updatePaper(PaperOBJ paper) throws SQLException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "Update paper set title = ?, abstract = ?, source = ?, year = ?, research_type = ?, empirical_type = ?, grade_level = ?, Impl_duration = ?, titleMD5 = ? where id = ? ;";
		
		pstmt = conn.prepareStatement(SQL);
		
		pstmt.setString(1, paper.getTitle());
		pstmt.setString(2, paper.getAbstractVal());
		pstmt.setString(3, paper.getSource());
		pstmt.setInt(4, paper.getPYear());
		pstmt.setString(5, paper.getResearchType());
		pstmt.setString(6, paper.getEmpiricalType());
		pstmt.setString(7, paper.getGradeLevel());
		pstmt.setInt(8, paper.getImplDuration());
		pstmt.setString(9, paper.getTitleMD5());
		pstmt.setInt(10, paper.getId());
		pstmt.executeUpdate();
		
		
	}
	
	/**
	 * delete the paper information in database
	 * @param paper_id paper id
	 * @throws SQLException
	 */
	public void deletePaper(int paper_id) throws SQLException {
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "DELETE FROM paper WHERE id = ? ;";
		
		pstmt = null;
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper_id);
		pstmt.executeUpdate();
	}
	
	/**
	 * Insert paper abstract info in database for query search
	 * @param paper
	 * @throws SQLException
	 * @throws IOException 
	 * @throws URISyntaxException 
	 */
	public void insertAbstract(PaperOBJ paper) throws SQLException, URISyntaxException, IOException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "INSERT INTO abstract VALUES ( ? , ? );";
		
		String abstractContent = paper.getAbstractVal();
		TreeSet<String> keywords = removeStopWord(abstractContent);
		
		for (String word : keywords) {
			
			pstmt = null;
			pstmt = conn.prepareStatement(SQL);
			pstmt.setInt(1, paper.getId());
			pstmt.setString(2, word);
			pstmt.executeUpdate();
			
		}
		
	}
	
	/**
	 * delete paper abstract info in database
	 * @param paper
	 * @throws SQLException
	 * @throws IOException 
	 * @throws URISyntaxException 
	 */
	public void deleteAbstract(PaperOBJ paper) throws SQLException, URISyntaxException, IOException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "DELETE FROM abstract WHERE paper_id = ? ;";
		
		pstmt = null;
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper.getId());
		pstmt.executeUpdate();
		
	}
	
	/**
	 * Insert paper authors info in database for query search
	 * @param paper
	 * @throws SQLException
	 */
	public void insertAuthor(PaperOBJ paper) throws SQLException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "INSERT INTO author VALUES ( ? , ? , ? , ?);";
		
		ArrayList<Author> authors = paper.getAuthors();
		
		for (Author author : authors) {
			
			pstmt = null;
			pstmt = conn.prepareStatement(SQL);
			pstmt.setInt(1, paper.getId());
			pstmt.setString(2, author.getFirstName().trim());
			pstmt.setString(3, author.getLastName().trim());
			pstmt.setInt(4, author.getAuthorOrder());
			pstmt.executeUpdate();
			
		}
		
	}
	
	/**
	 * delete paper authors info in database
	 * @param paper
	 * @throws SQLException
	 */
	public void deleteAuthor(PaperOBJ paper) throws SQLException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "DELETE FROM author WHERE paper_id = ? ;";
		
		pstmt = null;
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper.getId());
		pstmt.executeUpdate();
		
	}
	
	/**
	 * Insert paper subject info in database for query search
	 * @param paper
	 * @throws SQLException
	 */
	public void insertSubject(PaperOBJ paper) throws SQLException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "INSERT INTO subject VALUES ( ? , ? );";
		
		ArrayList<String> subjectCodes = paper.getSubject();
		
		for (String code : subjectCodes) {
			
			pstmt = null;
			pstmt = conn.prepareStatement(SQL);
			pstmt.setInt(1, paper.getId());
			pstmt.setString(2, code);
			pstmt.executeUpdate();
			
		}
		
	}
	
	/**
	 * Delete paper subject info in database
	 * @param paper
	 * @throws SQLException
	 */
	public void deleteSubject(PaperOBJ paper) throws SQLException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "DELETE FROM subject WHERE paper_id = ? ;";
		
		pstmt = null;
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper.getId());
		pstmt.executeUpdate();
		
	}
	
	/**
	 * Insert paper technology info in database for query search
	 * @param paper
	 * @throws SQLException
	 */
	public void insertTechnology(PaperOBJ paper) throws SQLException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "INSERT INTO technology VALUES ( ? , ? );";
		
		ArrayList<String> technologyCodes = paper.getTechnology();
		
		for (String code : technologyCodes) {
			
			pstmt = null;
			pstmt = conn.prepareStatement(SQL);
			pstmt.setInt(1, paper.getId());
			pstmt.setString(2, code);
			pstmt.executeUpdate();
			
		}
		
	}
	
	/**
	 * Delete paper technology info in database
	 * @param paper
	 * @throws SQLException
	 */
	public void deleteTechnology(PaperOBJ paper) throws SQLException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "DELETE FROM technology WHERE paper_id = ? ;";
		
		pstmt = null;
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper.getId());
		pstmt.executeUpdate();
		
	}
	
	/**
	 * Insert paper country info in database for query search
	 * @param paper
	 * @throws SQLException
	 */
	public void insertCountry(PaperOBJ paper) throws SQLException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "INSERT INTO country VALUES ( ? , ? );";
		
		ArrayList<String> countryList = paper.getCountry();
		for (String country : countryList) {
			
			pstmt = null;
			pstmt = conn.prepareStatement(SQL);
			pstmt.setInt(1, paper.getId());
			pstmt.setString(2, country);
			pstmt.executeUpdate();
			
		}
		
	}
	
	/**
	 * Delete paper country info in database
	 * @param paper
	 * @throws SQLException
	 */
	public void deleteCountry(PaperOBJ paper) throws SQLException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "DELETE FROM country WHERE paper_id = ? ;";
		
		pstmt = null;
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper.getId());
		pstmt.executeUpdate();
		
	}
	
	/**
	 * Insert paper abstract info in database for query search
	 * @param paper
	 * @throws SQLException
	 * @throws IOException 
	 * @throws URISyntaxException 
	 */
	public void insertTitle(PaperOBJ paper) throws SQLException, URISyntaxException, IOException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "INSERT INTO title VALUES ( ? , ? );";
		
		String title = paper.getTitle();
		TreeSet<String> keywords = removeStopWord(title);
		
		for (String word : keywords) {
			
			pstmt = null;
			pstmt = conn.prepareStatement(SQL);
			pstmt.setInt(1, paper.getId());
			pstmt.setString(2, word);
			pstmt.executeUpdate();
			
		}
		
	}
	
	private static TreeSet<String> stemmingWord(TreeSet<String> keywords) {
		
		TreeSet<String> result = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		
		for (String word : keywords) {
			String stemmingWord = stemmer.StemWordWithWordNet(word);
			if (stemmingWord != null) result.add(stemmingWord);
		}
		return result;
		
	}

	/**
	 * Delete paper title info in database
	 * @param paper
	 * @throws SQLException
	 */
	public void deleteTitle(PaperOBJ paper) throws SQLException{
		
		PreparedStatement pstmt = null;
		String SQL = "";
		SQL = "DELETE FROM title WHERE paper_id = ? ;";
		
		pstmt = null;
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper.getId());
		pstmt.executeUpdate();
		
	}
	
	/**
	 * update user new password and email
	 * @param newPassword
	 * @param email
	 * @param userID
	 * @throws SQLException
	 */
	public void changePasswordEmail(String newPassword, String email, String userID) throws SQLException {
		
		PreparedStatement pstmt = null;
		String SQL = "Update users set password = ?, email = ? where userID = ? ;";
		pstmt = conn.prepareStatement(SQL);
		pstmt.setString(1, newPassword);
		pstmt.setString(2, email);
		pstmt.setString(3, userID);
		// get result
		pstmt.executeUpdate();
	
	}
	
	/**
	 * To tokenize the input content to keywords, including remove the stop words 
	 * @param Word
	 * @return The HashSet of the keywords
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	public static TreeSet<String> removeStopWord(String Word) throws URISyntaxException, IOException {
		
		TreeSet<String> result = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		if (stopWords.size() == 0) stopWords = getStopWord();
		
		// get the qoute word
		for (String word : Word.split(" ")){
			word = word.trim().replaceAll("[`~!@#$%^&*()_+={}:;\"',<.>?/]", "");
	        word = word.replace("[", "");
	        word = word.replace("]", "");
			if (stopWords.contains(word.trim().toLowerCase())){
				continue;
			}
			if (!word.equals("")) result.add(word);
		}
		result = stemmingWord(result);
		
		return result;
	}

	/**
	 * get the stop word from stopWord.txt file
	 * @return The TreeSet of stop word 
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	private static TreeSet<String> getStopWord() throws URISyntaxException, IOException {
		
		TreeSet<String> result = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		
		String fullFileURL = DataManager.class.getResource("").toString();
		
		File list = new File(new URI(fullFileURL + "stopWord.txt"));
		FileReader fread = new FileReader(list);
		BufferedReader in = new BufferedReader(fread);
	    String str;
	    while ((str = in.readLine()) != null) {
	    	result.add(str.trim());
	    }
		in.close();
		fread.close();
		
		return result;
	}
	
}
