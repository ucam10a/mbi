package mpi.transaction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import mpi.orm.Author;
import mpi.orm.PaperOBJ;
import mpi.util.KeyWrodCollection;

/**
 * 
 * @author Long
 * The class which is responsible for query the data from database
 */
public class QueryManager {

	/** sql connection */
	public Connection conn;
	
	/** the auto complete suggestion words */
	public static HashMap<String, KeyWrodCollection> suggestWords = new HashMap<String, KeyWrodCollection>();
	
	public QueryManager(Connection _conn){
		this.conn = _conn;
	}
	
	public int getPaperId(int year, String title) throws SQLException{
		
		int id = 0;
		ResultSet rs = null;
		String SQL = "";
		PreparedStatement pstmt = null;
		SQL = "Select id from paper where year = ? and title = ? ;";
		
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, year);
		pstmt.setString(2, title);
		// get result
		rs = pstmt.executeQuery();
		
		// put result into classOBJs
		while (rs.next()) {
			id = rs.getInt(1);
		}
		
		return id;
	}

	public ArrayList<PaperOBJ> filterPaper(String SQL, ArrayList<Object> inputs, ArrayList<String> searchAttributes) throws Exception {
		
		ArrayList<PaperOBJ> results = new ArrayList<PaperOBJ>();
		
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		pstmt = conn.prepareStatement(SQL);
		int insertIdx = 1;
		for (Object obj : inputs) {
			if (obj.getClass().equals(java.lang.Integer.class)) {
				pstmt.setInt(insertIdx, (Integer)obj);
			}else if (obj.getClass().equals(java.lang.Double.class)){
				pstmt.setDouble(insertIdx, (Double)obj);
			}else if (obj.getClass().equals(java.lang.String.class)){
				pstmt.setString(insertIdx, (String)obj);
			}else {
				throw new Exception("Unknown input type for SQL!");
			}
			insertIdx++;
		}
		
		// get result
		rs = pstmt.executeQuery();
		
		// used to check the duplicate paper
		LinkedHashMap<Integer, PaperOBJ> papers = new LinkedHashMap<Integer, PaperOBJ>();
		
		// put result into classOBJs
		while (rs.next()) {
			
			PaperOBJ paperObj = new PaperOBJ();
			if (papers.containsKey(rs.getInt(1))){
				paperObj = papers.get(rs.getInt(1));
			} else {
				papers.put(rs.getInt(1), paperObj);
				paperObj.setId(rs.getInt(1));
				// add span color to title
				paperObj.setTitle(addSpan(rs.getString(2), searchAttributes, inputs, "title"));
				// add span color to abstract 
				paperObj.setAbstractVal(addSpan(rs.getString(3), searchAttributes, inputs, "abstract"));
				// add span color to source
				paperObj.setSource(addSpan(rs.getString(4), searchAttributes, inputs, "source"));
				paperObj.setPYear(rs.getInt(5));
				paperObj.setResearchType(rs.getString(6).toUpperCase());
				paperObj.setEmpiricalType(rs.getString(7).toUpperCase());
				paperObj.setGradeLevel(rs.getDouble(8) + "");
				paperObj.setImplDuration(rs.getInt(9));
			}
			
			if (!paperObj.getAuthorFullNames().contains(rs.getString(10) + rs.getString(11))){
				int order = rs.getInt(15);
				Author author = new Author();
				author.setFirstName(addSpan(rs.getString(10), searchAttributes, inputs, "author"));
				author.setLastName(addSpan(rs.getString(11), searchAttributes, inputs, "author"));
				author.setAuthorOrder(order);
				paperObj.addAuthor(author);
				paperObj.getAuthorFullNames().add(rs.getString(10) + rs.getString(11));
			}
			
			if (!paperObj.getTechCodes().contains(rs.getString(12))) {
				paperObj.getTechnology().add(addSpan(rs.getString(12), searchAttributes, inputs, "technology"));
				paperObj.getTechCodes().add(rs.getString(12));
			}
			
			if (!paperObj.getCountryCodes().contains(rs.getString(13))) {
				paperObj.getCountry().add(addSpan(rs.getString(13), searchAttributes, inputs, "country"));
				paperObj.getCountryCodes().add(rs.getString(13));
			}
			
			if (!paperObj.getSubjectCodes().contains(rs.getString(14))) {
				paperObj.getSubject().add(addSpan(rs.getString(14), searchAttributes, inputs, "subject"));
				paperObj.getSubjectCodes().add(rs.getString(14));
			}
		}
		
		// transfer to list
		for (int key : papers.keySet()){
			results.add(papers.get(key));
		}
		
		return results;
	}
	
	private String addSpan(String value,ArrayList<String> searchAttributes, ArrayList<Object> inputs, String target) {
		ArrayList<String> renderWords = new ArrayList<String>();
		for (String attr : searchAttributes) {
			if (attr.contains(target)) {
				renderWords = getInputsWord(inputs, searchAttributes, target);
				renderWords = CapitalFirstChar(renderWords);
				renderWords = removeLikeChar(renderWords);
				break;
			}
		}
		String newValue = value;
		for (String val : renderWords) {
			newValue = newValue.replaceAll("(?i)" + val, "<span style='background-color: yellow;'>" + val + "</span>");
		}
		return newValue;
	}

	private ArrayList<String> CapitalFirstChar(ArrayList<String> Words) {
		ArrayList<String> result = new ArrayList<String>();
		for (String word : Words) {
			result.add(word.substring(0, 1).toUpperCase() + word.substring(1));
		}
		return result;
	}

	private ArrayList<String> removeLikeChar(ArrayList<String> Words) {
		ArrayList<String> result = new ArrayList<String>();
		for (String word : Words) {
			word = word.replace("%", "");
			result.add(word);
		}
		return result;
	}
	
	private ArrayList<String> getInputsWord(ArrayList<Object> inputs, ArrayList<String> inputAttributes, String attribute) {
		
		ArrayList<String> result = new ArrayList<String>();
		for (int i = 0; i < inputAttributes.size(); i++) {
			if (inputAttributes.get(i).contains(attribute)){
				result.add(inputs.get(i).toString());
			}
		}
		return result;
	}
	
	public PaperOBJ getPaper(int paper_id) throws SQLException {
		
		PaperOBJ result = new PaperOBJ();
		ResultSet rs = null;
		String SQL = "";
		PreparedStatement pstmt = null;
		
		SQL = "Select id, title, abstract, source, year, research_type, empirical_type, grade_level, impl_duration from paper where id = ? ;";
		
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper_id);
		// get result
		rs = pstmt.executeQuery();
		
		// put result into classOBJs
		while (rs.next()) {
			result.setId(rs.getInt(1));
			result.setTitle(rs.getString(2));
			result.setAbstractVal(rs.getString(3));
			result.setSource(rs.getString(4));
			result.setPYear(rs.getInt(5));
			result.setResearchType(rs.getString(6));
			result.setEmpiricalType(rs.getString(7));
			result.setGradeLevel(rs.getString(8));
			result.setImplDuration(rs.getInt(9));
		}
		
		ArrayList<Author> authors = getPaperAuthors(paper_id);
		result.setAuthors(authors);
		
		ArrayList<String> subjects = getPaperSubjects(paper_id);
		result.setSubject(subjects);
		
		ArrayList<String> technologies = getPaperTechnologies(paper_id);
		result.setTechnology(technologies);
		
		ArrayList<String> countries = getPaperCountries(paper_id);
		result.setCountry(countries);
		
		return result;
		
		
	}

	public ArrayList<Author> getPaperAuthors(int paper_id) throws SQLException {
		
		ArrayList<Author> result = new ArrayList<Author>();
		ResultSet rs = null;
		String SQL = "";
		PreparedStatement pstmt = null;
		
        SQL = "Select first_name, last_name from author where paper_id = ? ;";
		
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper_id);
		// get result
		rs = pstmt.executeQuery();
		
		// put result into classOBJs
		while (rs.next()) {
			Author a = new Author();
			a.setFirstName(rs.getString(1));
			a.setLastName(rs.getString(2));
			result.add(a);
		}
		
		return result;
	}
	
	public ArrayList<String> getPaperTechnologies(int paper_id) throws SQLException {
		
		ArrayList<String> result = new ArrayList<String>();
		
		ResultSet rs = null;
		String SQL = "";
		PreparedStatement pstmt = null;
		
        SQL = "Select technology_code from technology where paper_id = ? ;";
		
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper_id);
		// get result
		rs = pstmt.executeQuery();
		
		// put result into classOBJs
		while (rs.next()) {
			result.add(rs.getString(1));
		}
		
		return result;
	}

	public ArrayList<String> getPaperSubjects(int paper_id) throws SQLException {
		
		ArrayList<String> result = new ArrayList<String>();
		
		ResultSet rs = null;
		String SQL = "";
		PreparedStatement pstmt = null;
		
        SQL = "Select subject_code from subject where paper_id = ? ;";
		
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper_id);
		// get result
		rs = pstmt.executeQuery();
		
		// put result into classOBJs
		while (rs.next()) {
			result.add(rs.getString(1));
		}
		
		return result;
	}
	
	public ArrayList<String> getPaperCountries(int paper_id) throws SQLException {

        ArrayList<String> result = new ArrayList<String>();
		
		ResultSet rs = null;
		String SQL = "";
		PreparedStatement pstmt = null;
		
        SQL = "Select country from country where paper_id = ? ;";
		
		pstmt = conn.prepareStatement(SQL);
		pstmt.setInt(1, paper_id);
		// get result
		rs = pstmt.executeQuery();
		
		// put result into classOBJs
		while (rs.next()) {
			result.add(rs.getString(1));
		}
		
		return result;
		
	}

	public boolean checkMD5(String md5, int execludeID) throws SQLException {
		
		ResultSet rs = null;
		String SQL = "";
		PreparedStatement pstmt = null;
		SQL = "Select id from paper where titleMD5 = ? ;";
		
		pstmt = conn.prepareStatement(SQL);
		pstmt.setString(1, md5);
		rs = pstmt.executeQuery();
		rs.last();
		
		if (rs.getRow() > 0){
			if (rs.getInt(1) != execludeID) return true;
		}
		return false;
	}

	public String getPassword(String userID) throws SQLException {
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL = "Select password " + 
		  "from users " + 
		  "where userID = ?;";
		pstmt = conn.prepareStatement(SQL);
		pstmt.setString(1, userID);
		// get result
		rs = pstmt.executeQuery();
		rs.next();
		if (rs.getRow() == 0) {
			return null;
		}else{
			return rs.getString(1);
		}
		
	}
	
}
