package mpi.util;

/**
 * @author Long
 * The class is used to store the error message and show on screen 
 */
public class MesgBox {
	
	/** error message */
	private String message;

	/** the message type */
	private boolean success = false;
	
	/**
	 * set the message type is successful or not
	 * @param success is the message successful 
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * is the success
	 * @return is success or not
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * set message word
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * get message word
	 * @return
	 */
	public String getMessage() {
		return message;
	}
}