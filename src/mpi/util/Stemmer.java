package mpi.util;

import net.didion.jwnl.*;
import net.didion.jwnl.data.*;
import net.didion.jwnl.dictionary.*;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;

/**
 * 
 * @author Long
 * Stemming word object, for example, books -> book, writing -> write, wrote -> write
 */
public class Stemmer {

	private Dictionary dic;
	private MorphologicalProcessor morph;
	private boolean IsInitialized = false;  
	public HashMap<String, String> AllWords = null;
	
	/**
	 * establishes connection to the WordNet database
	 */
	public Stemmer ()
	{
		AllWords = new HashMap<String, String>();
		
		try
		{
			String url = Stemmer.class.getResource("").toString();
			File file = new File(new URL(url + "file_properties.xml").toURI());
			
			// the file input stream is the path to your file_properties.xml file which you modified in the configurations step 
			JWNL.initialize(new FileInputStream(file)); 
			dic = Dictionary.getInstance();
			morph = dic.getMorphologicalProcessor();
			// ((AbstractCachingDictionary)dic).
			//	setCacheCapacity (10000);
			IsInitialized = true;
		}
		catch (FileNotFoundException e)
		{
			System.out.println ( "Error initializing Stemmer: JWNLproperties.xml not found" );
		}
		catch (JWNLException e)
		{
			System.out.println ( "Error initializing Stemmer: " + e.toString() );
		} 
		catch (MalformedURLException e) 
		{
			e.printStackTrace();
		} 
		catch (URISyntaxException e) 
		{
			e.printStackTrace();
		} 
		
	}
	
	public void Unload ()
	{ 
		dic.close();
		Dictionary.uninstall();
		JWNL.shutdown();
	}
	
	/**
	 * stems a word with wordnet
	 * @param word word to stem
	 * @return the stemmed word or null if it was not found in WordNet
	 */
	public String StemWordWithWordNet(String word)
	{
		word = word.toLowerCase();
		if ( !IsInitialized )
			return word;
		if ( word == null ) return null;
		if ( morph == null ) morph = dic.getMorphologicalProcessor();
		if ( containsNumbers (word) == true ) return word;
		
		IndexWord w;
		try
		{
			w = morph.lookupBaseForm( POS.VERB, word );
			if ( w != null ) return w.getLemma().toString ();
			w = morph.lookupBaseForm( POS.NOUN, word );
			if ( w != null ) return w.getLemma().toString();
			w = morph.lookupBaseForm( POS.ADJECTIVE, word );
			if ( w != null ) return w.getLemma().toString();
			w = morph.lookupBaseForm( POS.ADVERB, word );
			if ( w != null ) return w.getLemma().toString();
		} 
		catch ( JWNLException e )
		{
			// ignore
		}
		return null;
	}
	
	/**
	 * Stem a single word
	 * tries to look up the word in the AllWords HashMap
	 * If the word is not found it is stemmed with WordNet
	 * and put into AllWords
	 * 
	 * @param word word to be stemmed
	 * @return stemmed word
	 */
	public String Stem( String word )
	{
		// check if we already know the word
		String stemmedword = (String) AllWords.get( word );
		if ( stemmedword != null )
			return stemmedword; // return it if we already know it
		
		// don't check words with digits in them
		if ( containsNumbers (word) == true ) {
			stemmedword = null;
		}else{	// unknown word: try to stem it
			stemmedword = StemWordWithWordNet (word);
		}
		if ( stemmedword != null )
		{
			// word was recognized and stemmed with wordnet:
			// add it to hashmap and return the stemmed word
			AllWords.put( word, stemmedword );
			return stemmedword;
		}
		// word could not be stemmed by wordnet, 
		// thus it is no correct english word
		// just add it to the list of known words so 
		// we won't have to look it up again
		AllWords.put( word, word );
		return word;
	}
	
	private boolean containsNumbers(String word) {
		
		//The Character Wrapper Class in Java has methods isLetter() and isDigit()
		for (int n = 0; n < word.length() ; n++){
			Character c = word.charAt(n);
			if (Character.isDigit(c)) return true; //If it finds a digit, it returns true and exits the function
		}
		return false; //After checking all the characters, if no digits were found it returns false
	
	}
	
	public static void main(String[] args)
	{
		Stemmer stemmer = new Stemmer();
		System.out.println(stemmer.StemWordWithWordNet("conceptual"));
		
	}
	
}
