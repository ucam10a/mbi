package mpi.util;

import java.util.TreeSet;
import java.sql.Timestamp;

/**
 * @author Long
 * The class is used to store the most common keywords, and is used offer to auto complete suggestion
 */
public class KeyWrodCollection {

	/** the collection of keywords */
	private TreeSet<String> suggestWords;
	
	/** record the time of storing keywords, if time is over 30 minutes, then system will restore the keywords again */
	private Timestamp recordTime;

	public KeyWrodCollection(Timestamp _recordTime, TreeSet<String> _suggestWords){
		suggestWords = _suggestWords;
		recordTime = _recordTime;
	} // constructor
	
	/**
	* set the most common keywords collection
	* @param suggestWords keyword collection
	* @return void
	*/
	public void setSuggestWords(TreeSet<String> suggestWords) {
		this.suggestWords = suggestWords;
	}

	/**
	* get the most common keyword collection
	* @return 
	*/
	public TreeSet<String> getSuggestWords() {
		return suggestWords;
	}

	/**
	* set the keyword storing time
	* @param recordTime Timestamp of storing time
	* @return void
	*/
	public void setRecordTime(Timestamp recordTime) {
		this.recordTime = recordTime;
	}

	/**
	* get the keyword storing time
	* @return storing time
	*/
	public Timestamp getRecordTime() {
		return recordTime;
	}
	
}
