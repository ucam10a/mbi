package mpi.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import mpi.orm.Author;
import mpi.orm.PaperOBJ;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadXlsxFile {

	/** the excel file of title */
	private ArrayList<String> titles;
	
	/** the paper object collection */
	private ArrayList<PaperOBJ> papers;
	
	/**
	 * read the file input stream to create the java object
	 * @param input
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws InvalidFormatException 
	 * @throws NoSuchAlgorithmException 
	 */
	public ReadXlsxFile(FileInputStream input) throws InvalidFormatException, IOException, NoSuchAlgorithmException {
		
		Workbook workbook = WorkbookFactory.create(input);
		
		papers = new ArrayList<PaperOBJ>();
		titles = new ArrayList<String>();
		
		//System.out.println("Number Of Sheets" + workbook.getNumberOfSheets());
	    Sheet sheet = workbook.getSheetAt(0);
	    
	    //System.out.println("Number Of Rows:" + sheet.getLastRowNum());
	    int totalRows = sheet.getLastRowNum();

	    for (int i = 0; i < totalRows; i++){
	    	
	    	Row row = sheet.getRow(i);
	    	PaperOBJ paper = new PaperOBJ();
	    	if (row != null){
	    		
	    		int totalCells = row.getLastCellNum();
		    	
				for (int j = 0; j < totalCells; j++){
		    		
		    		if (row.getCell(j) != null) {
		    			int type = row.getCell(j).getCellType();
					    //System.out.println("Cell type:" + row.getCell(0).getCellType());
					    //System.out.println("Cell Value:" + row.getCell(0).getNumericCellValue());
					    if (i == 0) {
					    	titles.add(row.getCell(j).getStringCellValue());
					    } else {
					    	setValue(paper, j, row, type);
					    } // if
		    		}
		    		
		    	} // for
	    	}
	    	
	    	// get title MD5 hash code
	    	paper.setTitleMD5(getMD5(paper.getTitle()));
	    	
	    	if (i != 0 && row != null && paper.getPYear() != 0) papers.add(paper);
	    	
	    } // for
	
	} // constructor
	
	/**
	 * set paper object value by the column index number
	 * @param paper paper object 
	 * @param colidx column index number
	 * @param row row object
	 */
	public void setValue(PaperOBJ paper, int colidx, Row row, int type){
		
		if (colidx == MBIConstant.PYEAR){
    		paper.setPYear((int) row.getCell(0).getNumericCellValue());
    	}else if (colidx == MBIConstant.AUTHORS){
    		paper.setAuthors(row.getCell(colidx).getStringCellValue().trim());
    	}else if (colidx == MBIConstant.TITLE){
    		paper.setTitle(row.getCell(colidx).getStringCellValue().trim());
    	}else if (colidx == MBIConstant.SOURCE){
    		paper.setSource(row.getCell(colidx).getStringCellValue().trim());
    	}else if (colidx == MBIConstant.ABSTRACT){
    		paper.setAbstractVal(row.getCell(colidx).getStringCellValue().trim());
    	}else if (colidx == MBIConstant.RESEARCH_TYPE){
    		paper.setResearchType(row.getCell(colidx).getStringCellValue().trim());
    	}else if (colidx == MBIConstant.EMPIRICAL_TYPE){
    		paper.setEmpiricalType(row.getCell(colidx).getStringCellValue().trim());
    	}else if (colidx == MBIConstant.TECHNOLOGY){
    		paper.setTechnology(row.getCell(colidx).getStringCellValue().trim());
    	}else if (colidx == MBIConstant.COUNTRY){
    		paper.setCountry(row.getCell(colidx).getStringCellValue().trim());
    	}else if (colidx == MBIConstant.GRADE_LEVEL){
    		if (type == 1){
    			paper.setGradeLevel(row.getCell(colidx).getStringCellValue().trim());
    		} else {
    			paper.setGradeLevel(row.getCell(colidx).getNumericCellValue() + "");
    		}
    	}else if (colidx == MBIConstant.SUBJECT){
    		paper.setSubject(row.getCell(colidx).getStringCellValue().trim());
    	}else if (colidx == MBIConstant.IMPL_DURATION){
    		paper.setImplDuration((int) row.getCell(colidx).getNumericCellValue());
    	}
		
	}
	
	public static String getMD5(String title) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		
		byte[] defaultBytes = title.getBytes("utf-8");
		MessageDigest algorithm = MessageDigest.getInstance("MD5");
		algorithm.reset();
		algorithm.update(defaultBytes);
		byte messageDigest[] = algorithm.digest();
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < messageDigest.length; i++) {
			hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
		}
		return hexString.toString();
	
	}

	/**
	 * return the titles of this excel file
	 * @return titles
	 */
	public ArrayList<String> getTitles() 
	{
		return titles;
	}
	
	/**
	 * return the list paper object 
	 * @return list of paper object
	 */
	public ArrayList<PaperOBJ> getPapers() 
	{
		return papers;
	}
	
	/**
	 * test
	 * @param argus
	 * @throws IOException 
	 * @throws InvalidFormatException 
	 * @throws NoSuchAlgorithmException 
	 */
	public static void main(String[] argus) throws InvalidFormatException, IOException, NoSuchAlgorithmException 
	{
		
		File file = new File("D:/data for web_slee.xlsx");
		FileInputStream input = new FileInputStream(file);
		
		ReadXlsxFile reader = new ReadXlsxFile(input);
		ArrayList<PaperOBJ> papers = reader.getPapers();
		System.out.println("paper size = " + papers.size());
		
		for (PaperOBJ paper : papers) {
			
			System.out.println("title = " + paper.getTitle());
			System.out.println("PYear = " + paper.getPYear());
			int i = 1;
			for (Author author : paper.getAuthors()){
				System.out.println(i + "th Author = " + author.getLastName() + ", " + author.getFirstName());
				i++;
			}
			System.out.println("subject = " + paper.getSubject());
			System.out.println("impl duration = " + paper.getImplDuration());
		}
		
	}
	
}
