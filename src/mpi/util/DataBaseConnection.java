package mpi.util;

import java.sql.*;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * @author Long
 * The class is used to get the MySQL connection from connection pool
 */
public class DataBaseConnection {
	
	/** connection object */
	private Connection conn = null;

	public DataBaseConnection() {
		try {
			
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource)ctx.lookup(MBIToolConfig.getEnv());
			conn = ds.getConnection();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
	} // constructor

	/**
	* get the connection of database
	* @return connection object
	*/
	public Connection getConnection() {
		return this.conn;
	}

	/**
	* close the connection of database
	* @return void
	*/
	public void close() {
		try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
	
