package mpi.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * @author Long
 * The class is used to define some environment value 
 */
public class MBIToolConfig {
	
	/** property file path */
	private static String propertyFile;
    
	/** define the data source */
    private static String env;
    
    /** the logger */
	private static Logger logger;
    
	public MBIToolConfig(){
		// not implement
	} // constructor
	
	/**
	 * get the data source environment
	 * @return env
	 */
	public static String getEnv() {
		if (env == null){
			try {
				ClassLoader loader = MBIToolConfig.class.getClassLoader();
				String url = loader.getResource("MBIToolConfig.properties").toString();
				propertyFile = url;
				URL propURL;
				propURL = new URL(propertyFile);
				File file = new File(propURL.toURI());
				Properties prop = new Properties();
				prop.load(new FileInputStream(file));
				return prop.getProperty("env");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			return env;
		}
		
		return env;
	}

	/**
	 * get the logger
	 * @return logger
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws URISyntaxException 
	 */
	public static Logger getLogger(String contextPath){
		
		if (logger == null){
			
			try {
				
				ClassLoader loader = MBIToolConfig.class.getClassLoader();
				String classPath = loader.getResource("").toString();
				int end = classPath.lastIndexOf(contextPath);
				String base = classPath.substring(0, end) + contextPath + "/log/";
				Properties logp = new Properties();
				String logPropPath = base + "Log4j.properties";
				String logFilePath = base + "web.log";
				File logPropFile = new File(new URL(logPropPath).toURI());
				File logfile = new File(new URL(logFilePath).toURI());
				logp.load(new FileInputStream(logPropFile));
				if (logp.getProperty("log4j.appender.A2.File") == null || logp.getProperty("log4j.appender.A2.File").equals("")){
					logp.setProperty("log4j.appender.A2.File", logfile.getAbsolutePath());
					logp.store(new FileOutputStream(logPropFile), null);
				}
				PropertyConfigurator.configure(logp);
			    logger = Logger.getLogger("J2EE");
			    
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			
		}
		
		return logger;
	}
}
