package mpi.util;

/**
 * @author Long
 * The class is used to define the constant value 
 */
public class MBIConstant {

	/** PYear column number */
	public static final int PYEAR = 0;
	
	/** Authors column number */
	public static final int AUTHORS = 1;
	
	/** title column number */
	public static final int TITLE = 2;
	
	/** source column number */
	public static final int SOURCE = 3;
	
	/** abstract column number */
	public static final int ABSTRACT = 4;
	
	/** research type column number */
	public static final int RESEARCH_TYPE = 5;
	
	/** empirical type column number */
	public static final int EMPIRICAL_TYPE = 6;
	
	/** technology column number */
	public static final int TECHNOLOGY = 7;
	
	/** region column number */
	public static final int COUNTRY = 8;
	
	/** grade level column number */
	public static final int GRADE_LEVEL = 9;
	
	/** subject column number */
	public static final int SUBJECT = 10;
	
	/** implement duration column number */
	public static final int IMPL_DURATION = 11;
	
	/** login error message */
	public static String LOGIN_ERROR = "Please login first! if you don't have account, apply a new one. ";
	
	/** not authorized */
	public static String NOT_AUTHORIZED = "You are not authorized to do this action.";

	/** data added successfully */
	public static String DATA_ADDED = "Data added successfully!";
	
	/** data deleted successfully */
	public static String DATA_DELETED = "Data delete successfully!";
	
	/** data updated successfully */
	public static String DATA_UPDATED = "Data update successfully!";
	
	/** import data is not xlsx */
	public static String DATA_IMPORT_ERROR = "The input file is not xlsx";
	
	/** system error */
	public static String SYSTEM_ERROR = "System error!";
	
	/** number format error */
	public static String NOT_NUMBER = " is not a number";
	
	/** password error */
	public static String PASSWORD_ERROR = "The username or password is incorrect!";

	/** year is outside the limit */
	public static String YEAR_OUTSIDE_LIMIT = "The system contains publications from 1980-2010, please try again.";

	/** duplicate paper title */
	public static String DUPLICATE_PAPER_TITLE = "There is a duplicate title!";
	
}
