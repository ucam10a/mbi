package mpi.action;

import java.sql.Connection;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import mpi.orm.Author;
import mpi.orm.PaperOBJ;

import mpi.transaction.DataManager;
import mpi.transaction.QueryManager;
import mpi.util.DataBaseConnection;
import mpi.util.MBIConstant;
import mpi.util.MBIToolConfig;
import mpi.util.MesgBox;
import mpi.util.ReadXlsxFile;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ActionContext;

/**
 * @author Long
 * The action class is used to insert new data into database
 */
public class Insert extends ActionSupport{
  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String paperId;
	private String year; 
	private String title;
	private String abstractVal;
	private String[] firstName;
	private String[] lastName;
	private String source;
	private String researchType;
	private String empiricalType;
	private String[] subjectCode;
	private String[] countryCode;
	private String[] technologyCode;
	private String gradeLevel;
	private String implDuration;
	private ArrayList<PaperOBJ> papers;
	private Vector<MesgBox> loginError;
	private Vector<MesgBox> vecError;
		  
	public String execute()throws Exception{		
		
		DataBaseConnection dbcn = null;
		dbcn = new DataBaseConnection();
		Connection conn = dbcn.getConnection(); 
		MesgBox message = new MesgBox();
		loginError = new Vector<MesgBox>();
		vecError = new Vector<MesgBox>();
		papers = new ArrayList<PaperOBJ>();
		
		@SuppressWarnings("rawtypes")
		Map session = ActionContext.getContext().getSession();
		
		if (session.get("login") != "true"){
			session.remove("login");
			session.remove("userID");
			session.remove("accountType");
			message.setMessage(MBIConstant.LOGIN_ERROR);
			message.setSuccess(false);
			vecError.add(message);
			dbcn.close();
			return ERROR;
		}
		
		try {
			
			/*
			System.out.println("paperId = " + paperId);
			System.out.println("year = " + year);
			System.out.println("title = " + title);
			System.out.println("abstractVal = " + abstractVal);
			for (String fname : firstName) {
				System.out.println("firstName = " + fname);
			}
			for (String lanme : lastName) {
				System.out.println("lastName = " + lanme);
			}
			for (String tcode : technologyCode) {
				System.out.println("tcode = " + tcode);
			}
			for (String scode : subjectCode) {
				System.out.println("scode = " + scode);
			}
			for (String ccode : countryCode) {
				System.out.println("ccode = " + ccode);
			}
			System.out.println("source = " + source);
			System.out.println("researchType = " + researchType);
			System.out.println("empiricalType = " + empiricalType);
			System.out.println("gradeLevel = " + gradeLevel);
			System.out.println("implDuration = " + implDuration);
			*/
			
			DataManager datamgr = new DataManager(conn);
			QueryManager querymgr = new QueryManager(conn);
			boolean error = false;
			
			int Pyear = 0;
			try {
				Pyear = Integer.parseInt(year);
			}catch (Exception e){
				e.printStackTrace();
				message.setMessage(year + MBIConstant.NOT_NUMBER);
				message.setSuccess(false);
				vecError.add(message);
				error = true;
			}
			
			int ImpleDur = 0;
			try {
				ImpleDur = Integer.parseInt(implDuration);
			}catch (Exception e){
				e.printStackTrace();
				message.setMessage(implDuration + MBIConstant.NOT_NUMBER);
				message.setSuccess(false);
				vecError.add(message);
				error = true;
			}
			String md5 = ReadXlsxFile.getMD5(title);
			
			// check duplicate title
			boolean duplicate = querymgr.checkMD5(md5, -1);
			if (duplicate){
				message.setMessage("There is a duplicate title!");
				message.setSuccess(false);
				vecError.add(message);
				error = true;
			}
			
			PaperOBJ paper = new PaperOBJ();
			paper.setPYear(Pyear);
			paper.setTitle(title);
			paper.setAbstractVal(abstractVal);
			paper.setSource(source);
			paper.setResearchType(researchType);
			paper.setEmpiricalType(empiricalType);
			paper.setGradeLevel(gradeLevel);
			paper.setImplDuration(ImpleDur);
			paper.setTitleMD5(md5);
			papers.add(paper);
			
			if (error) {
				return ERROR;
			}
			
			ArrayList<Author> authors = new ArrayList<Author>();
			if (firstName != null){
				for (int i = 0; i < firstName.length; i++) {
					Author a = new Author();
					a.setFirstName(firstName[i]);
					a.setLastName(lastName[i]);
					authors.add(a);
				}
			}else{
				message.setMessage("There is no author!");
				message.setSuccess(false);
				vecError.add(message);
				return ERROR;
			}
			paper.setAuthors(authors);
			
			ArrayList<String> techCodes = new ArrayList<String>();
			if (technologyCode != null){
				for (String code : technologyCode) {
					techCodes.add(code);
				}
			}else {
				techCodes.add("NA");
			}
			paper.setTechnology(techCodes);
			
			ArrayList<String> subjectCodes = new ArrayList<String>();
			if (subjectCode != null){
				for (String code : subjectCode) {
					subjectCodes.add(code);
				}
			}else{
				subjectCodes.add("NA");
			}
			paper.setSubject(subjectCodes);
			
			ArrayList<String> countryCodes = new ArrayList<String>();
			if (countryCode != null){
				for (String code : countryCode) {
					countryCodes.add(code);
				}
			}else{
				countryCodes.add("NA");
			}
			paper.setCountry(countryCodes);

			// set Auto Commit off 
			conn.setAutoCommit(false);
			
			// insert paper
			datamgr.insertPaper(paper);
			
			// get paper id
			int paperId = querymgr.getPaperId(paper.getPYear(), paper.getTitle());
			paper.setId(paperId);
			
			// insert abstract
			datamgr.insertAbstract(paper);
			
			// insert author
			datamgr.insertAuthor(paper);
			
			// insert country
			datamgr.insertCountry(paper);
			
			// insert subject
			datamgr.insertSubject(paper);
			
			// insert technology
			datamgr.insertTechnology(paper);
			
			// insert title
			datamgr.insertTitle(paper);
			
			message.setMessage("Insert successfully!");
			message.setSuccess(true);
			vecError.add(message);
			conn.commit();
			return SUCCESS;
			
		}catch (Exception e) {
			e.printStackTrace();
			message = new MesgBox();
			message.setMessage(e.toString());
			loginError.add(message);
			vecError.add(message);
			message.setSuccess(false);

			// record error log
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			Logger logger = MBIToolConfig.getLogger(contextPath);
			logger.error(e.toString());
			return ERROR;
			
		}finally{
			dbcn.close();
		}
	}
  
	public Vector<MesgBox> getLoginError() {  
		return loginError;      
	}
	    
	public void setLoginError(Vector<MesgBox> loginError) {     
		this.loginError = loginError;      
	}
	
	public void setVecError(Vector<MesgBox> vecError) {
		this.vecError = vecError;
	}

	public Vector<MesgBox> getVecError() {
		return vecError;
	}

	public void setPaperId(String paperId) {
		this.paperId = paperId;
	}

	public String getPaperId() {
		return paperId;
	}

	public void setPapers(ArrayList<PaperOBJ> papers) {
		this.papers = papers;
	}

	public ArrayList<PaperOBJ> getPapers() {
		return papers;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setAbstractVal(String abstractVal) {
		this.abstractVal = abstractVal;
	}

	public String getAbstractVal() {
		return abstractVal;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getYear() {
		return year;
	}

	public void setFirstName(String[] firstName) {
		this.firstName = firstName;
	}

	public String[] getFirstName() {
		return firstName;
	}

	public void setLastName(String[] lastName) {
		this.lastName = lastName;
	}

	public String[] getLastName() {
		return lastName;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSource() {
		return source;
	}

	public void setResearchType(String researchType) {
		this.researchType = researchType;
	}

	public String getResearchType() {
		return researchType;
	}

	public void setEmpiricalType(String empiricalType) {
		this.empiricalType = empiricalType;
	}

	public String getEmpiricalType() {
		return empiricalType;
	}

	public void setGradeLevel(String gradeLevel) {
		this.gradeLevel = gradeLevel;
	}

	public String getGradeLevel() {
		return gradeLevel;
	}

	public void setImplDuration(String implDuration) {
		this.implDuration = implDuration;
	}

	public String getImplDuration() {
		return implDuration;
	}

	public void setSubjectCode(String[] subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String[] getSubjectCode() {
		return subjectCode;
	}

	public void setCountryCode(String[] countryCode) {
		this.countryCode = countryCode;
	}

	public String[] getCountryCode() {
		return countryCode;
	}

	public void setTechnologyCode(String[] technologyCode) {
		this.technologyCode = technologyCode;
	}

	public String[] getTechnologyCode() {
		return technologyCode;
	}
  
}