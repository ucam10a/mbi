package mpi.action;

import java.sql.Connection;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import mpi.transaction.QueryManager;
import mpi.util.DataBaseConnection;
import mpi.util.MBIConstant;
import mpi.util.MBIToolConfig;
import mpi.util.MesgBox;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ActionContext;

/**
 * @author Long
 * The action class is used to check the user login information
 */
public class Login extends ActionSupport{
  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userID = null;	
	private String password = null;
	private Vector<MesgBox> loginError;
	private Vector<MesgBox> vecError;
		  
	@SuppressWarnings("unchecked")
	public String execute()throws Exception{		
		DataBaseConnection dbcn = null;
		dbcn = new DataBaseConnection();
		Connection conn = dbcn.getConnection(); 
		loginError = new Vector<MesgBox>();
		vecError = new Vector<MesgBox>();
		userID = userID.toLowerCase();
		MesgBox message = new MesgBox();
		
		QueryManager querymgr = new QueryManager(conn);
		
		try {
			
			String checkword = querymgr.getPassword(userID);
			if (checkword == null){
				message = new MesgBox();  
				message.setMessage(MBIConstant.PASSWORD_ERROR);
				loginError.add(message);
				vecError.add(message);
				message.setSuccess(false);
				return ERROR;
			}
			
			if((!getPassword().equals(checkword))){
				
				message = new MesgBox();  
				message.setMessage(MBIConstant.PASSWORD_ERROR);
				loginError.add(message);
				vecError.add(message);
				message.setSuccess(false);
				return ERROR;
			
			}else{
				
				@SuppressWarnings("rawtypes")
				Map session = ActionContext.getContext().getSession();
				session.put("login", "true");
				session.put("userID", userID);
				return SUCCESS;
			}				
		}catch (Exception e) {
			e.printStackTrace();
			message = new MesgBox();
			message.setMessage(e.toString());
			loginError.add(message);
			vecError.add(message);
			message.setSuccess(false);

			// record error log
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			Logger logger = MBIToolConfig.getLogger(contextPath);
			logger.error(e.toString());
			return ERROR;
		
		}finally{
			dbcn.close();
		}
	}
  
	public String getUserID(){
		return userID;
	}
  
	public void setUserID(String userID){
		this.userID = userID;  
	}
  
	public String getPassword(){
		return password;  
	}	  

	public void setPassword(String password){	
		this.password = password;    
	}
	
	public Vector<MesgBox> getLoginError() {  
		return loginError;      
	}
	    
	public void setLoginError(Vector<MesgBox> loginError) {     
		this.loginError = loginError;      
	}
	
	public void setVecError(Vector<MesgBox> vecError) {
		this.vecError = vecError;
	}

	public Vector<MesgBox> getVecError() {
		return vecError;
	}
  
}