package mpi.action;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import mpi.orm.PaperOBJ;
import mpi.transaction.DataManager;
import mpi.transaction.QueryManager;
import mpi.util.DataBaseConnection;
import mpi.util.MBIConstant;
import mpi.util.MBIToolConfig;
import mpi.util.MesgBox;
import mpi.util.ReadXlsxFile;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @author Long
 * The action class is used to add the new data from excel file
 */
public class AddNewDataFromXlsx extends ActionSupport {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private File file;
	private Vector<MesgBox> vecError;

	public String execute() {
		
		DataBaseConnection dbcn = null;
		dbcn = new DataBaseConnection();
		Connection conn = dbcn.getConnection();
		vecError = new Vector<MesgBox>();
		MesgBox message = new MesgBox();
		
		try {
			
			@SuppressWarnings("rawtypes")
			Map session = ActionContext.getContext().getSession();
			
			if (session.get("login") != "true"){
				session.remove("login");
				session.remove("userID");
				session.remove("accountType");
				message.setMessage(MBIConstant.NOT_AUTHORIZED);
				message.setSuccess(false);
				vecError.add(message);
				dbcn.close();
				return ERROR;
			}
			
			// set Auto Commit off 
			conn.setAutoCommit(false);
			
			DataManager datamgr = new DataManager(conn);
			QueryManager querymgr = new QueryManager(conn);
			
			FileInputStream input = new FileInputStream(file);
			ReadXlsxFile reader = new ReadXlsxFile(input);
			ArrayList<PaperOBJ> papers = reader.getPapers();
			
			// check any empty value in paper
			for (PaperOBJ paper : papers) {
				if (paper.getTechnology().size() == 0){
					paper.getTechnology().add("NA");
				}
				if (paper.getCountry().size() == 0){
					paper.getCountry().add("NA");
				}
				if (paper.getSubject().size() == 0){
					paper.getSubject().add("NA");
				}
			}
			
			for (PaperOBJ paper : papers) {
				
				// insert paper
				datamgr.insertPaper(paper);
				
				// get paper id
				int paperId = querymgr.getPaperId(paper.getPYear(), paper.getTitle());
				paper.setId(paperId);
				
				// insert abstract
				datamgr.insertAbstract(paper);
				
				// insert author
				datamgr.insertAuthor(paper);
				
				// insert country
				datamgr.insertCountry(paper);
				
				// insert subject
				datamgr.insertSubject(paper);
				
				// insert technology
				datamgr.insertTechnology(paper);
				
				// insert title
				datamgr.insertTitle(paper);
				
			}
			
			message = new MesgBox();
			message.setMessage(MBIConstant.DATA_ADDED);
			message.setSuccess(true);
			vecError.add(message);
			
			// commit
			conn.commit();
			return SUCCESS; 
		
		}
		catch (IllegalArgumentException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			//put error message into vecError�Ashow on error.jsp
			message = new MesgBox();
			message.setMessage(MBIConstant.DATA_IMPORT_ERROR);
			message.setSuccess(false);
			vecError.add(message);
			
			// record error log
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			Logger logger = MBIToolConfig.getLogger(contextPath);
			logger.error(e.toString());
			
			return ERROR;
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			//put error message into vecError�Ashow on error.jsp
			message = new MesgBox();
			message.setMessage(e.toString());
			message.setSuccess(false);
			vecError.add(message);
			
			// record error log
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			Logger logger = MBIToolConfig.getLogger(contextPath);
			logger.error(e.toString());
			return ERROR;
		
		}finally{
			dbcn.close();
		}
	}

	public void setVecError(Vector<MesgBox> vecError) {
		this.vecError = vecError;
	}

	public Vector<MesgBox> getVecError() {
		return vecError;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public File getFile() {
		return file;
	}

}

