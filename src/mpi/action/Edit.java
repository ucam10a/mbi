package mpi.action;

import java.sql.Connection;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import mpi.orm.PaperOBJ;
import mpi.transaction.QueryManager;
import mpi.util.DataBaseConnection;
import mpi.util.MBIConstant;
import mpi.util.MBIToolConfig;
import mpi.util.MesgBox;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ActionContext;

/**
 * @author Long
 * The action class is used to get edit data then show to user
 */
public class Edit extends ActionSupport{
  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String paperId = "";
	private ArrayList<PaperOBJ> papers;
	private Vector<MesgBox> loginError;
	private Vector<MesgBox> vecError;
		  
	public String execute()throws Exception{		
		
		DataBaseConnection dbcn = null;
		dbcn = new DataBaseConnection();
		Connection conn = dbcn.getConnection(); 
		MesgBox message = new MesgBox();
		loginError = new Vector<MesgBox>();
		vecError = new Vector<MesgBox>();
		papers = new ArrayList<PaperOBJ>();
		
		@SuppressWarnings("rawtypes")
		Map session = ActionContext.getContext().getSession();
		
		if (session.get("login") != "true"){
			session.remove("login");
			session.remove("userID");
			session.remove("accountType");
			message.setMessage(MBIConstant.LOGIN_ERROR);
			message.setSuccess(false);
			vecError.add(message);
			dbcn.close();
			return ERROR;
		}
		
		try {
			
			// set Auto Commit off 
			conn.setAutoCommit(false);
			
			QueryManager querymgr = new QueryManager(conn);
			int paper_id = Integer.parseInt(paperId);
			PaperOBJ paper = querymgr.getPaper(paper_id);
			papers.add(paper);
			
			return SUCCESS;
			
		}catch (Exception e) {
			e.printStackTrace();
			message = new MesgBox();
			message.setMessage(e.toString());
			loginError.add(message);
			vecError.add(message);
			message.setSuccess(false);
			
			// record error log
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			Logger logger = MBIToolConfig.getLogger(contextPath);
			logger.error(e.toString());
			return ERROR;
		
		}finally{
			dbcn.close();
		}
	}
  
	public Vector<MesgBox> getLoginError() {  
		return loginError;      
	}
	    
	public void setLoginError(Vector<MesgBox> loginError) {     
		this.loginError = loginError;      
	}
	
	public void setVecError(Vector<MesgBox> vecError) {
		this.vecError = vecError;
	}

	public Vector<MesgBox> getVecError() {
		return vecError;
	}

	public void setPaperId(String paperId) {
		this.paperId = paperId;
	}

	public String getPaperId() {
		return paperId;
	}

	public void setPapers(ArrayList<PaperOBJ> papers) {
		this.papers = papers;
	}

	public ArrayList<PaperOBJ> getPapers() {
		return papers;
	}
  
}