package mpi.action;

import java.sql.Connection;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import mpi.transaction.DataManager;
import mpi.transaction.QueryManager;
import mpi.util.DataBaseConnection;
import mpi.util.MBIConstant;
import mpi.util.MBIToolConfig;
import mpi.util.MesgBox;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @author Long
 * The action class is used to change the user's password
 */
public class ChangePass extends ActionSupport {
	
	private static final long serialVersionUID = 1L;
	private String oldPassword;
	private String newPassword;
	private String confirmNewPassword;
	private String email;
	private Vector<MesgBox> vecError;
	private Vector<MesgBox> passwordError;
	private Vector<MesgBox> newPsswordError;
	private String messageType;

	
	public String execute() {
		DataBaseConnection dbcn = null;
		dbcn = new DataBaseConnection();
		Connection conn = dbcn.getConnection();
		vecError = new Vector<MesgBox>();
		passwordError = new Vector<MesgBox>();
		newPsswordError = new Vector<MesgBox>();
		boolean error = false;
		MesgBox message = new MesgBox();
		
		@SuppressWarnings("rawtypes")
		Map session = ActionContext.getContext().getSession();
		
		if (session.get("login") != "true"){
			session.remove("login");
			session.remove("userID");
			session.remove("accountType");
			message = new MesgBox();
			message.setMessage(MBIConstant.LOGIN_ERROR);
			vecError.add(message);
			messageType = "error";
			dbcn.close();
			return LOGIN;
		}

		String userID = (String)session.get("userID");
		DataManager datamgr = new DataManager(conn);
		QueryManager querymgr = new QueryManager(conn);
		
		try {
			
			if(checkNull(oldPassword)){
				message = new MesgBox();
				message.setMessage("Password can not be empty!");
				passwordError.add(message);
				error = true;
			}
			if(checkNull(newPassword)){
				message = new MesgBox();
				message.setMessage("New password can not be empty!");
				newPsswordError.add(message);
				error = true;
			}
			if(!newPassword.equals(confirmNewPassword)){
				message = new MesgBox();
				message.setMessage("Confirm password does not match confirm password!");
				newPsswordError.add(message);
				error = true;
			}
			if (newPassword.length() <= 5){
				message = new MesgBox();
				message.setMessage("Password must be more than 6 characters!");
				newPsswordError.add(message);
				error = true;
			}
			if (error == true) {
				return "input";
			}
			
			String oldPass = querymgr.getPassword(userID);
			if(oldPass != null && !oldPass.equals(oldPassword)){
				message = new MesgBox();
				message.setMessage("The old password doesn't match! please try again!");
				passwordError.add(message);
				return "input";
			}
			
			// change password
			datamgr.changePasswordEmail(newPassword, email, userID);
			
			message = new MesgBox();
			message.setSuccess(true);
			message.setMessage("Password is modified successfully!");
			vecError.add(message);
			System.out.println("successfully");
			return SUCCESS;
			
		} 
		catch (Exception e) {
			e.printStackTrace();
			//put error message into vecError�Ashow on error.jsp
			message = new MesgBox();
			message.setMessage(e.toString());
			vecError.add(message);
			
			// record error log
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			Logger logger = MBIToolConfig.getLogger(contextPath);
			logger.error(e.toString());
			
			return ERROR;
		
		}finally{
			dbcn.close();
		}
	}

    public Vector<MesgBox> getVecError() {  
        return vecError;  
    }
    
    public void setVecError(Vector<MesgBox> vecError) {  
        this.vecError = vecError;  
    }  
    
    public static boolean checkNull(String input){
    	if (input.equals("") || input == null) return true;
    	return false;
    }

	public void setPasswordError(Vector<MesgBox> passwordError) {
		this.passwordError = passwordError;
	}

	public Vector<MesgBox> getPasswordError() {
		return passwordError;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setConfirmNewPassword(String confirmNewPassword) {
		this.confirmNewPassword = confirmNewPassword;
	}

	public String getConfirmNewPassword() {
		return confirmNewPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPsswordError(Vector<MesgBox> newPsswordError) {
		this.newPsswordError = newPsswordError;
	}

	public Vector<MesgBox> getNewPsswordError() {
		return newPsswordError;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}
	
}
    