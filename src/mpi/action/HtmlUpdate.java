package mpi.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.net.URI;
import java.net.URL;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import mpi.util.MBIConstant;
import mpi.util.MBIToolConfig;
import mpi.util.MesgBox;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ActionContext;

/**
 * @author Long
 * The action class is used to get edit html page
 */
public class HtmlUpdate extends ActionSupport{
  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String type = "";
	private String htmlCode = "";
	private Vector<MesgBox> loginError;
	private Vector<MesgBox> vecError;
		  
	public String execute()throws Exception{		
		
		MesgBox message = new MesgBox();
		loginError = new Vector<MesgBox>();
		vecError = new Vector<MesgBox>();
		
		@SuppressWarnings("rawtypes")
		Map session = ActionContext.getContext().getSession();
		
		if (session.get("login") != "true"){
			session.remove("login");
			session.remove("userID");
			session.remove("accountType");
			message.setMessage(MBIConstant.NOT_AUTHORIZED);
			message.setSuccess(false);
			vecError.add(message);
			return ERROR;
		}
		
		try {
			
			// get contextPath ex.TKSS
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			
			ClassLoader loader = HtmlUpdate.class.getClassLoader();
			
			String classPath = loader.getResource("").toString();
			int end = classPath.lastIndexOf(contextPath);
			String baseURI = classPath.substring(0, end) + contextPath + "/";
			
			String url = MBIToolConfig.class.getResource("").toString();
			String propertyFile = url + "MPIToolConfig.properties";
			
			URL propURL;
			propURL = new URL(propertyFile);
			File file = new File(propURL.toURI());
			Properties prop = new Properties();
			prop.load(new FileInputStream(file));
			String jspFile = prop.getProperty(type);
			
			// Open the file that is the first
			// command line parameter
			File jspfile = new File(new URI(baseURI + jspFile));
			FileWriter fstream = new FileWriter(jspfile);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(htmlCode);
			// Close the output stream
			out.close();
			
			return SUCCESS;
			
		}catch (Exception e) {
			e.printStackTrace();
			message = new MesgBox();
			message.setMessage(e.toString());
			loginError.add(message);
			vecError.add(message);
			message.setSuccess(false);

			// record error log
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			Logger logger = MBIToolConfig.getLogger(contextPath);
			logger.error(e.toString());
			return ERROR;
		}
	}
  
	public Vector<MesgBox> getLoginError() {  
		return loginError;      
	}
	    
	public void setLoginError(Vector<MesgBox> loginError) {     
		this.loginError = loginError;      
	}
	
	public void setVecError(Vector<MesgBox> vecError) {
		this.vecError = vecError;
	}

	public Vector<MesgBox> getVecError() {
		return vecError;
	}

	public void setHtmlCode(String htmlCode) {
		this.htmlCode = htmlCode;
	}

	public String getHtmlCode() {
		return htmlCode;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

  
}