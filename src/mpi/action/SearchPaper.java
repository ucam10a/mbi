package mpi.action;

import java.sql.Connection;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import mpi.orm.PaperOBJ;
import mpi.transaction.DataManager;
import mpi.transaction.QueryManager;
import mpi.util.DataBaseConnection;
import mpi.util.MBIConstant;
import mpi.util.MBIToolConfig;
import mpi.util.MesgBox;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @author Long
 * The action class is used to search data
 */
public class SearchPaper extends ActionSupport{
  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id = 0;
	private String fromYear;
	private String toYear;
	private String title;
	private String abstractVal;
	private String author;
	private String source;
	private String researchType;
	private String empiricalType;
	private String technology;
	private String country;
	private String subject;
	private double grade;
	private String fromImplDuration;
	private String toImplDuration;
	private ArrayList<PaperOBJ> papers;
	private ArrayList<String> inputAttributes;
	private Vector<MesgBox> loginError;
	private Vector<MesgBox> vecError;
	private String papers_id;
		  
	public String execute()throws Exception{		
		
		DataBaseConnection dbcn = null;
		dbcn = new DataBaseConnection();
		Connection conn = dbcn.getConnection(); 
		loginError = new Vector<MesgBox>();
		vecError = new Vector<MesgBox>();
		setPapers(new ArrayList<PaperOBJ>());
		papers_id = "";
		MesgBox message = new MesgBox();
		
		try {
			
			// set Auto Commit off 
			conn.setAutoCommit(false);
			
			QueryManager querymgr = new QueryManager(conn);
			int tableInputIdx = 0;
			ArrayList<Object> inputs = new ArrayList<Object>();
		    inputAttributes = new ArrayList<String>();
			
			String selectAttribute = "Select P.id, P.title, P.abstract , P.source, P.year, P.research_type, P.empirical_type, P.grade_level, P.impl_duration, AU.first_name, AU.last_name, TECH.technology_code, C.country, S.subject_code, AU.order ";
			String selectTable = "paper P ";
			String filterStatement = "AU.paper_id = P.id and TECH.paper_id = P.id and C.paper_id = P.id and S.paper_id = P.id";
			
			if (id != 0) {
				filterStatement = addFilter(filterStatement, "P.id = ? ");
				inputs.add(id);
				inputAttributes.add("id");
			}
			
			int fromYr = -1;
			int toYr = -1;
			try {
				if (fromYear != null && !fromYear.equals("")) fromYr = Integer.parseInt(fromYear);
				if (toYear != null && !toYear.equals("")) toYr = Integer.parseInt(toYear);
			}catch (Exception e) {
				message = new MesgBox();
				message.setMessage(fromYear + " or " + toYear + message);
				loginError.add(message);
				vecError.add(message);
				message.setSuccess(false);
				return ERROR;
			}
			
			if (fromYr != -1 || toYr != -1) {
				
				if (fromYr == -1) fromYr = 1980;
				if (toYr == -1) toYr = 2010;
				
				// swap fromYr and toYr if order not correct
				if (fromYr > toYr){
					int temp = fromYr;
					fromYr = toYr;
					toYr = temp;
				}
				
				// deal with error less than 1980 and over than 2010 
				if (toYr > 2010 || fromYr < 1980) {
					message = new MesgBox();
					message.setMessage(MBIConstant.YEAR_OUTSIDE_LIMIT);
					vecError.add(message);
					message.setSuccess(false);
					return ERROR;
				}
				
				filterStatement = addFilter(filterStatement, "P.year >= ? and P.year <= ?");
				inputs.add(fromYr);
				inputAttributes.add("year");
				inputs.add(toYr);
				inputAttributes.add("year");
			}
			
			if (title != null && !title.equalsIgnoreCase("")) {
				title = title.trim();
				TreeSet<String> keywords = new TreeSet<String>();
				int start = title.indexOf('"');
				if (start != -1) {
					int end = title.indexOf('"', start+1);
					String quoteWord = title.substring(start+1, end);
					selectTable = addTable(selectTable, "title T");
					String filter = "";
					filter = addQuoteWord("T", "title", quoteWord, inputs, inputAttributes);
					filterStatement = addFilter(filterStatement, filter);
				} else {
					keywords = DataManager.removeStopWord(title);
					if (keywords.size() != 0) {
						selectTable = addTable(selectTable, "title T");
						String filter = "";
						filter = addKeyword("T", "title_keyword", keywords, inputs, inputAttributes);
						filterStatement = addFilter(filterStatement, filter);
					}
				}
			}
			
			if (abstractVal != null && !abstractVal.equalsIgnoreCase("")) {
				abstractVal = abstractVal.trim();
				TreeSet<String> keywords = new TreeSet<String>();
				int start = abstractVal.indexOf('"');
				if (start != -1) {
					int end = abstractVal.indexOf('"', start+1);
					String quoteWord = abstractVal.substring(start+1, end);
					selectTable = addTable(selectTable, "abstract AB");
					String filter = "";
					filter = addQuoteWord("AB", "abstract", quoteWord, inputs, inputAttributes);
					filterStatement = addFilter(filterStatement, filter);
				} else {
					keywords = DataManager.removeStopWord(abstractVal);
					if (keywords.size() != 0) {
						selectTable = addTable(selectTable, "abstract AB");
						String filter = "";
						filter = addKeyword("AB", "abstract_word", keywords, inputs, inputAttributes);
						filterStatement = addFilter(filterStatement, filter);
					}
				}
			}
			
			if (author != null && !author.equals("")) {
				author = author.trim();
				selectTable = addTable(selectTable, "(Select * from author AS AU1 where AU1.paper_id IN ( Select paper_id from author where (first_name LIKE ? OR last_name LIKE ?) )) AS AU ");
				inputs.add(tableInputIdx, "%" + author + "%");
				inputAttributes.add(tableInputIdx, "author");
				tableInputIdx++;
				inputs.add(tableInputIdx, "%" + author + "%");
				inputAttributes.add(tableInputIdx, "author");
				tableInputIdx++;
			} else {
				selectTable = addTable(selectTable, "author AU ");
			}
			
			if (source != null && !source.equals("")) {
				source = source.trim();
				filterStatement = addFilter(filterStatement, "P.source LIKE ? ");
				inputs.add("%" + source + "%");
				inputAttributes.add("source");
			}
			
			if (researchType != null && !researchType.equals("")) {
				researchType = researchType.trim();
				filterStatement = addFilter(filterStatement, "P.research_type = ? ");
				inputs.add(researchType);
				inputAttributes.add("researchType");
			}
			
			if (empiricalType != null && !empiricalType.equals("")) {
				empiricalType = empiricalType.trim();
				filterStatement = addFilter(filterStatement, "P.empirical_type = ? ");
				inputs.add(empiricalType);
				inputAttributes.add("empiricalType");
			}
			
			if (technology != null && !technology.equals("")) {
				technology = technology.trim();
				selectTable = addTable(selectTable, "(Select * from technology AS T1 where T1.paper_id IN ( Select paper_id from technology where technology_code = ? )) AS TECH ");
				inputs.add(tableInputIdx, technology);
				inputAttributes.add(tableInputIdx, "technology");
				tableInputIdx++;
			} else {
				selectTable = addTable(selectTable, "technology AS TECH");
			}
			
			if (country != null && !country.equals("")) {
				country = country.trim();
				selectTable = addTable(selectTable, "(Select * from country AS C1 where C1.paper_id IN ( Select paper_id from country where country = ? )) AS C ");
				inputs.add(tableInputIdx, country);
				inputAttributes.add(tableInputIdx, "country");
				tableInputIdx++;
			} else {
				selectTable = addTable(selectTable, "country AS C");
			}
			
			if (subject != null && !subject.equals("")) {
				subject = subject.trim();
				selectTable = addTable(selectTable, "(Select * from subject AS S1 where S1.paper_id IN ( Select paper_id from subject where subject_code = ? )) AS S ");
				inputs.add(tableInputIdx, subject);
				inputAttributes.add(tableInputIdx, "subject");
				tableInputIdx++;
			} else {
				selectTable = addTable(selectTable, "subject AS S");
			}
			
			if (grade != 0.0) {
				filterStatement = addFilter(filterStatement, "P.grade_level = ? ");
				inputs.add(grade);
				inputAttributes.add("grade");
			}
			
			int fromImpl = -1;
			int toImpl = -1; 
			try {
				if (fromImplDuration != null && !fromImplDuration.equals("")) fromImpl = Integer.parseInt(fromImplDuration);
				if (toImplDuration != null && !toImplDuration.equals("")) toImpl = Integer.parseInt(toImplDuration);
			}catch (Exception e) {
				message = new MesgBox();
				message.setMessage(fromImplDuration + " or " + toImplDuration + MBIConstant.NOT_NUMBER);
				loginError.add(message);
				vecError.add(message);
				message.setSuccess(false);
				return ERROR;
			}
			
			if (fromImpl != -1 || toImpl != -1) {
				if (fromImpl == -1) fromImpl = 0;
				if (toImpl == -1) toImpl = Integer.MAX_VALUE;
				filterStatement = addFilter(filterStatement, "P.impl_duration >= ? and P.impl_duration <= ?");
				inputs.add(fromImpl);
				inputAttributes.add("implDuration");
				inputs.add(toImpl);
				inputAttributes.add("implDuration");
			}
			
			String SQL = selectAttribute + " " + 
			  "from " + selectTable + " " + 
			  "where " + filterStatement + " ;";
			
			System.out.println("SQL = \n" + SQL);
			
			papers = querymgr.filterPaper(SQL, inputs, inputAttributes);
			
			// add the papers_id for export excel file
			for (PaperOBJ paper : papers) {
				papers_id = papers_id + paper.getId() + ", ";
			}
			
			return SUCCESS;
			
		}catch (Exception e) {
			e.printStackTrace();
			message = new MesgBox();
			message.setMessage(e.toString());
			loginError.add(message);
			vecError.add(message);
			message.setSuccess(false);

			// record error log
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			Logger logger = MBIToolConfig.getLogger(contextPath);
			logger.error(e.toString());
			return ERROR;
		
		}finally{
			dbcn.close();
		}

	}

	private String addQuoteWord(String table, String attribute, String quoteWord, ArrayList<Object> inputs, ArrayList<String> inputAttributes) {
		
		String result = "";
		result = table + ".paper_id = P.id";
		result = result + " and P." + attribute + " LIKE ? ";
		inputs.add("%" + quoteWord + "%");
		inputAttributes.add(attribute);
		return result;
		
	}
	
	private String addKeyword(String table, String attribute, TreeSet<String> keywords, ArrayList<Object> inputs, ArrayList<String> inputAttributes) {
		
		String result = "";
		result = table + ".paper_id = P.id";
		int total = 1;
		boolean start = true;
		for (String keyword : keywords){
			
			
			System.out.println("keyword = " + keyword);
			
			
			if (start) {
				result = result + " and ( " + table + "." + attribute + " = ? ";
				inputs.add(keyword);
				inputAttributes.add(attribute);
				start = false;
			} else {
				result = result + table + "." + attribute + " = ? ";
				inputs.add(keyword);
				inputAttributes.add(attribute);
			}
			total++;
			if (total <= (keywords.size())) result = result + "OR ";
		}
		if (total != 0) result = result + ")";
		return result;
		
	}

	private String addFilter(String filterStatement, String filter) {
		String result = filterStatement;
		if (filterStatement != null && !filterStatement.equals("")) {
			result = filterStatement + " and " + filter;
		} else {
			result = filterStatement + filter;
		}
		return result;
	}

	private String addTable(String selectTable, String table) {
		
		String result = "";
	    if (selectTable == null || selectTable.equals("")){
	    	result = table;
	    } else {
	    	result = selectTable + ", " + table;
	    }
		return result;
	
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setAbstractVal(String abstractVal) {
		this.abstractVal = abstractVal;
	}

	public String getAbstractVal() {
		return abstractVal;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSource() {
		return source;
	}

	public void setResearchType(String researchType) {
		this.researchType = researchType;
	}

	public String getResearchType() {
		return researchType;
	}

	public void setEmpiricalType(String empiricalType) {
		this.empiricalType = empiricalType;
	}

	public String getEmpiricalType() {
		return empiricalType;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getTechnology() {
		return technology;
	}

	public void setGrade(double grade) {
		this.grade = grade;
	}

	public double getGrade() {
		return grade;
	}

	public void setPapers(ArrayList<PaperOBJ> papers) {
		this.papers = papers;
	}

	public ArrayList<PaperOBJ> getPapers() {
		return papers;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountry() {
		return country;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthor() {
		return author;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setInputAttributes(ArrayList<String> inputAttributes) {
		this.inputAttributes = inputAttributes;
	}

	public ArrayList<String> getInputAttributes() {
		return inputAttributes;
	}

	public void setVecError(Vector<MesgBox> vecError) {
		this.vecError = vecError;
	}

	public Vector<MesgBox> getVecError() {
		return vecError;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setFromYear(String fromYear) {
		this.fromYear = fromYear;
	}

	public String getFromYear() {
		return fromYear;
	}

	public void setToYear(String toYear) {
		this.toYear = toYear;
	}

	public String getToYear() {
		return toYear;
	}

	public void setFromImplDuration(String fromImplDuration) {
		this.fromImplDuration = fromImplDuration;
	}

	public String getFromImplDuration() {
		return fromImplDuration;
	}

	public void setToImplDuration(String toImplDuration) {
		this.toImplDuration = toImplDuration;
	}

	public String getToImplDuration() {
		return toImplDuration;
	}

	public void setPapers_id(String papers_id) {
		this.papers_id = papers_id;
	}

	public String getPapers_id() {
		return papers_id;
	}

}