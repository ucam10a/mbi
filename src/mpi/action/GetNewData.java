package mpi.action;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import mpi.orm.PaperOBJ;
import mpi.util.MBIConstant;
import mpi.util.MBIToolConfig;
import mpi.util.MesgBox;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ActionContext;

/**
 * @author Long
 * This is used to show draft insert form for user
 */
public class GetNewData extends ActionSupport{
  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<PaperOBJ> papers;
	private Vector<MesgBox> loginError;
	private Vector<MesgBox> vecError;
		  
	public String execute()throws Exception{		
		
		MesgBox message = new MesgBox();
		loginError = new Vector<MesgBox>();
		vecError = new Vector<MesgBox>();
		papers = new ArrayList<PaperOBJ>();
		
		@SuppressWarnings("rawtypes")
		Map session = ActionContext.getContext().getSession();
		
		if (session.get("login") != "true"){
			session.remove("login");
			session.remove("userID");
			session.remove("accountType");
			message.setMessage(MBIConstant.LOGIN_ERROR);
			message.setSuccess(false);
			vecError.add(message);
			return ERROR;
		}
		
		try {
			
			PaperOBJ paper = new PaperOBJ();
			paper = new PaperOBJ();
			paper.setPYear(0);
			paper.setTitle("Inset title here");
			paper.setAbstractVal("Insert abstract here");
			paper.setSource("Insert source here");
			paper.setImplDuration(0);
			papers.add(paper);
			
			return SUCCESS;
			
		}catch (Exception e) {
			e.printStackTrace();
			message = new MesgBox();
			message.setMessage(e.toString());
			loginError.add(message);
			vecError.add(message);
			message.setSuccess(false);

			// record error log
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			Logger logger = MBIToolConfig.getLogger(contextPath);
			logger.error(e.toString());
			return ERROR;
		}
	}
  
	public Vector<MesgBox> getLoginError() {  
		return loginError;      
	}
	    
	public void setLoginError(Vector<MesgBox> loginError) {     
		this.loginError = loginError;      
	}
	
	public void setVecError(Vector<MesgBox> vecError) {
		this.vecError = vecError;
	}

	public Vector<MesgBox> getVecError() {
		return vecError;
	}

	public void setPapers(ArrayList<PaperOBJ> papers) {
		this.papers = papers;
	}

	public ArrayList<PaperOBJ> getPapers() {
		return papers;
	}
	
}