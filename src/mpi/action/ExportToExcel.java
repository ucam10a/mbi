package mpi.action;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import mpi.orm.Author;
import mpi.orm.PaperOBJ;
import mpi.transaction.QueryManager;
import mpi.util.DataBaseConnection;
import mpi.util.MBIToolConfig;
import mpi.util.MesgBox;

import jxl.*;
import jxl.write.*;

import com.opensymphony.xwork2.ActionSupport;
 
/**
 * @author Long
 * The action class is used to export the statistic data of the class to excel file
 */
public class ExportToExcel extends ActionSupport{
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String papers_id;
	private InputStream fileInputStream;
	private String fileName;
	private Vector<MesgBox> vecError;
	private Vector<MesgBox> logError;
	private String messageType;
	
	public String execute(){
		
		DataBaseConnection dbcn = null;
		dbcn = new DataBaseConnection();
		Connection conn = dbcn.getConnection();
		QueryManager querymgr = new QueryManager(conn);
		fileName = "SearchResult.xls";
		
		try {
			
			List<PaperOBJ> paperOBJs = new ArrayList<PaperOBJ>();
			
			// get data from database
			String[] paper_ids = papers_id.split(","); 
			for (String paper_id : paper_ids) {
				if (paper_id != null && !paper_id.equals("")) {
					int id = Integer.parseInt(paper_id.trim());
					paperOBJs.add(querymgr.getPaper(id));
				}
			}
			
			String filename = "SearchResult.xls";
			filename = filename.replace(':', '-').replace(' ', '_');
			
			// create excel file 
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			
			// create workbook and sheet
			WritableWorkbook workbook = Workbook.createWorkbook(out); 
			WritableSheet papers = workbook.createSheet("Papers", 0); 
			
			// write paper sheet title
			Label Pyear = new Label(0, 0, "Pyear");
			papers.addCell(Pyear); 
			Label Authors = new Label(1, 0, "Authors");
			papers.addCell(Authors);
			Label Title = new Label(2, 0, "Title");
			papers.addCell(Title);
			Label Source = new Label(3, 0, "Source");
			papers.addCell(Source);
			Label Abstract = new Label(4, 0, "Abstract");
			papers.addCell(Abstract);
			Label Research_Type = new Label(5, 0, "Research Type");
			papers.addCell(Research_Type);
			Label Empirical_Type = new Label(6, 0, "Empirical Type");
			papers.addCell(Empirical_Type);
			Label Technology = new Label(7, 0, "Technology");
			papers.addCell(Technology);
			Label Country_Region = new Label(8, 0, "Country/Region");
			papers.addCell(Country_Region);
			Label Grade_Level = new Label(9, 0, "Grade Level");
			papers.addCell(Grade_Level);
			Label Subject = new Label(10, 0, "Subject");
			papers.addCell(Subject);
			Label Impl_Duration = new Label(11, 0, "Impl Duration");
			papers.addCell(Impl_Duration);
			
		    
			int counter = 1; // start row
			// write student info
			for (PaperOBJ paper : paperOBJs){
				
				// add year
				Label paper_year = new Label(0, counter, paper.getPYear() + " ");
				papers.addCell(paper_year);
				
				// add authors info
				String authors = "";					
				for (Author au : paper.getAuthors()){
					authors = authors + au.getLastName() + ", " + au.getFirstName() + "; ";
				}
				Label paper_authors = new Label(1, counter, authors);
				papers.addCell(paper_authors);
				
				// add paper title
				Label paper_title = new Label(2, counter, paper.getTitle());
				papers.addCell(paper_title);
				
				// add paper source
				Label paper_source = new Label(3, counter, paper.getSource());
				papers.addCell(paper_source);
				
				// add paper abstract
				Label paper_abstract = new Label(4, counter, paper.getAbstractVal());
				papers.addCell(paper_abstract);
				
				// add paper research type
				Label paper_research_type = new Label(5, counter, paper.getResearchType());
				papers.addCell(paper_research_type);
				
				// add paper empirical type
				Label paper_empirical_type = new Label(6, counter, paper.getEmpiricalType());
				papers.addCell(paper_empirical_type);
				
				// add technology
				String technology = "";
				for (String code : paper.getTechnology()){
					technology = technology + code + ", ";
				}
				Label paper_technologies = new Label(7, counter, technology);
				papers.addCell(paper_technologies);
				
				// add country
				String country = "";
				for (String code : paper.getCountry()){
					country = country + code + ", ";
				}
				Label paper_countries = new Label(8, counter, country);
				papers.addCell(paper_countries);
				
				// add grade level
				Label paper_grades = new Label(9, counter, paper.getGradeLevel());
				papers.addCell(paper_grades);
				
				// add subject
				String subject = "";
				for (String code : paper.getSubject()){
					subject = subject + code + ", ";
				}
				Label paper_subjects = new Label(10, counter, subject);
				papers.addCell(paper_subjects);
				
				// add implement duration
				Label paper_implDuration = new Label(11, counter, paper.getImplDuration() + "");
				papers.addCell(paper_implDuration);
				
				counter++;
			}
			
			// close workbook
			workbook.write(); 
			workbook.close();
			
			//fileInputStream = new FileInputStream(myClass);
			fileInputStream = new ByteArrayInputStream(out.toByteArray());
			
			return SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			//put error message into vecError�Ashow on error.jsp
			MesgBox message = new MesgBox();
			message.setMessage(e.toString());
			vecError = new Vector<MesgBox>();
			vecError.add(message);
			
			// record error log
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			Logger logger = MBIToolConfig.getLogger(contextPath);
			logger.error(e.toString());
			return ERROR;
		
		}finally{
			dbcn.close();
		}
		
	}
	
	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setVecError(Vector<MesgBox> vecError) {
		this.vecError = vecError;
	}

	public Vector<MesgBox> getVecError() {
		return vecError;
	}

	public void setLogError(Vector<MesgBox> logError) {
		this.logError = logError;
	}

	public Vector<MesgBox> getLogError() {
		return logError;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setPapers_id(String papers_id) {
		this.papers_id = papers_id;
	}

	public String getPapers_id() {
		return papers_id;
	}
	
}
