package mpi.action;

import java.sql.Connection;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import mpi.orm.Author;
import mpi.orm.PaperOBJ;
import mpi.transaction.DataManager;
import mpi.transaction.QueryManager;
import mpi.util.DataBaseConnection;
import mpi.util.MBIConstant;
import mpi.util.MBIToolConfig;
import mpi.util.MesgBox;
import mpi.util.ReadXlsxFile;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ActionContext;

/**
 * @author Long
 * The action class is used to update the edit data in database
 */
public class Update extends ActionSupport{
  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String paperId;
	private String year; 
	private String title;
	private String abstractVal;
	private String[] firstName;
	private String[] lastName;
	private String source;
	private String researchType;
	private String empiricalType;
	private String[] subjectCode;
	private String[] countryCode;
	private String[] technologyCode;
	private String gradeLevel;
	private String implDuration;
	private ArrayList<PaperOBJ> papers;
	private Vector<MesgBox> loginError;
	private Vector<MesgBox> vecError;
		  
	public String execute()throws Exception{		
		
		DataBaseConnection dbcn = null;
		dbcn = new DataBaseConnection();
		Connection conn = dbcn.getConnection(); 
		MesgBox message = new MesgBox();
		loginError = new Vector<MesgBox>();
		vecError = new Vector<MesgBox>();
		papers = new ArrayList<PaperOBJ>();
		
		@SuppressWarnings("rawtypes")
		Map session = ActionContext.getContext().getSession();
		
		if (session.get("login") != "true"){
			session.remove("login");
			session.remove("userID");
			session.remove("accountType");
			message.setMessage(MBIConstant.LOGIN_ERROR);
			message.setSuccess(false);
			vecError.add(message);
			dbcn.close();
			return ERROR;
		}
		
		try {
			
			/*
			System.out.println("paperId = " + paperId);
			System.out.println("year = " + year);
			System.out.println("title = " + title);
			System.out.println("abstractVal = " + abstractVal);
			for (String fname : firstName) {
				System.out.println("firstName = " + fname);
			}
			for (String lanme : lastName) {
				System.out.println("lastName = " + lanme);
			}
			for (String tcode : technologyCode) {
				System.out.println("tcode = " + tcode);
			}
			for (String scode : subjectCode) {
				System.out.println("scode = " + scode);
			}
			for (String ccode : countryCode) {
				System.out.println("ccode = " + ccode);
			}
			System.out.println("source = " + source);
			System.out.println("researchType = " + researchType);
			System.out.println("empiricalType = " + empiricalType);
			System.out.println("gradeLevel = " + gradeLevel);
			System.out.println("implDuration = " + implDuration);
			*/
			
			DataManager datamgr = new DataManager(conn);
			QueryManager querymgr = new QueryManager(conn);
			boolean error = false;
			
			int Pyear = 0;
			try {
				Pyear = Integer.parseInt(year);
			}catch (Exception e){
				e.printStackTrace();
				message.setMessage(year + MBIConstant.NOT_NUMBER);
				message.setSuccess(false);
				vecError.add(message);
				error = true;
			}
			
			int ImpleDur = 0;
			try {
				ImpleDur = Integer.parseInt(implDuration);
				if (ImpleDur < 0){
					message.setMessage(implDuration + MBIConstant.NOT_NUMBER);
					message.setSuccess(false);
					vecError.add(message);
					error = true;
				}
			}catch (Exception e){
				e.printStackTrace();
				message.setMessage(implDuration + MBIConstant.NOT_NUMBER);
				message.setSuccess(false);
				vecError.add(message);
				error = true;
			}
			int paper_id = Integer.parseInt(paperId);
			
			String md5 = ReadXlsxFile.getMD5(title);
			boolean duplicate = querymgr.checkMD5(md5, paper_id);
			if (duplicate){
				message.setMessage(MBIConstant.DUPLICATE_PAPER_TITLE);
				message.setSuccess(false);
				vecError.add(message);
				error = true;
			}
			
			PaperOBJ paper = new PaperOBJ();
			paper.setId(paper_id);
			paper.setPYear(Pyear);
			paper.setTitle(title);
			paper.setAbstractVal(abstractVal);
			paper.setSource(source);
			paper.setResearchType(researchType);
			paper.setEmpiricalType(empiricalType);
			paper.setGradeLevel(gradeLevel);
			paper.setImplDuration(ImpleDur);
			paper.setTitleMD5(ReadXlsxFile.getMD5(title));
			papers.add(paper);
			
			if (error) {
				return ERROR;
			}
			
			ArrayList<Author> authors = new ArrayList<Author>();
			for (int i = 0; i < firstName.length; i++) {
				Author a = new Author();
				a.setFirstName(firstName[i]);
				a.setLastName(lastName[i]);
				authors.add(a);
			}
			paper.setAuthors(authors);
			
			ArrayList<String> techCodes = new ArrayList<String>();
			for (String code : technologyCode) {
				techCodes.add(code);
			}
			paper.setTechnology(techCodes);
			
			ArrayList<String> subjectCodes = new ArrayList<String>();
			for (String code : subjectCode) {
				subjectCodes.add(code);
			}
			paper.setSubject(subjectCodes);
			
			ArrayList<String> countryCodes = new ArrayList<String>();
			for (String code : countryCode) {
				countryCodes.add(code);
			}
			paper.setCountry(countryCodes);
			
			// set Auto Commit off 
			conn.setAutoCommit(false);
			
			// update paper
			datamgr.updatePaper(paper);
			
			// delete abstract
			datamgr.deleteAbstract(paper);
			// insert abstract
			datamgr.insertAbstract(paper);
			
			// delete author
			datamgr.deleteAuthor(paper);
			// insert author
			datamgr.insertAuthor(paper);
			
			// delete country
			datamgr.deleteCountry(paper);
			// insert country
			datamgr.insertCountry(paper);
			
			// delete subject
			datamgr.deleteSubject(paper);
			// insert subject
			datamgr.insertSubject(paper);
			
			// delete technology
			datamgr.deleteTechnology(paper);
			// insert technology
			datamgr.insertTechnology(paper);
			
			// delete title
			datamgr.deleteTitle(paper);
			// insert title
			datamgr.insertTitle(paper);
			
			// set the message
			message.setMessage(MBIConstant.DATA_UPDATED);
			message.setSuccess(true);
			vecError.add(message);
			conn.commit();
			return SUCCESS;
			
		}catch (Exception e) {
			e.printStackTrace();
			message = new MesgBox();
			message.setMessage(e.toString());
			loginError.add(message);
			vecError.add(message);
			message.setSuccess(false);

			// record error log
			HttpServletRequest req = ServletActionContext.getRequest();
			String contextPath = req.getContextPath().split("/")[1];
			Logger logger = MBIToolConfig.getLogger(contextPath);
			logger.error(e.toString());
			return ERROR;
		
		}finally{
			dbcn.close();
		}
	}
  
	public Vector<MesgBox> getLoginError() {  
		return loginError;      
	}
	    
	public void setLoginError(Vector<MesgBox> loginError) {     
		this.loginError = loginError;      
	}
	
	public void setVecError(Vector<MesgBox> vecError) {
		this.vecError = vecError;
	}

	public Vector<MesgBox> getVecError() {
		return vecError;
	}

	public void setPaperId(String paperId) {
		this.paperId = paperId;
	}

	public String getPaperId() {
		return paperId;
	}

	public void setPapers(ArrayList<PaperOBJ> papers) {
		this.papers = papers;
	}

	public ArrayList<PaperOBJ> getPapers() {
		return papers;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setAbstractVal(String abstractVal) {
		this.abstractVal = abstractVal;
	}

	public String getAbstractVal() {
		return abstractVal;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getYear() {
		return year;
	}

	public void setFirstName(String[] firstName) {
		this.firstName = firstName;
	}

	public String[] getFirstName() {
		return firstName;
	}

	public void setLastName(String[] lastName) {
		this.lastName = lastName;
	}

	public String[] getLastName() {
		return lastName;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSource() {
		return source;
	}

	public void setResearchType(String researchType) {
		this.researchType = researchType;
	}

	public String getResearchType() {
		return researchType;
	}

	public void setEmpiricalType(String empiricalType) {
		this.empiricalType = empiricalType;
	}

	public String getEmpiricalType() {
		return empiricalType;
	}

	public void setGradeLevel(String gradeLevel) {
		this.gradeLevel = gradeLevel;
	}

	public String getGradeLevel() {
		return gradeLevel;
	}

	public void setImplDuration(String implDuration) {
		this.implDuration = implDuration;
	}

	public String getImplDuration() {
		return implDuration;
	}

	public void setSubjectCode(String[] subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String[] getSubjectCode() {
		return subjectCode;
	}

	public void setCountryCode(String[] countryCode) {
		this.countryCode = countryCode;
	}

	public String[] getCountryCode() {
		return countryCode;
	}

	public void setTechnologyCode(String[] technologyCode) {
		this.technologyCode = technologyCode;
	}

	public String[] getTechnologyCode() {
		return technologyCode;
	}
  
}